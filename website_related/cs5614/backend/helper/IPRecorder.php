<?php
function Getip()
{
    $ipAddress = $_SERVER['REMOTE_ADDR'];
    if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
        $exploded = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
        $ipAddress = array_pop($exploded);
    }
    return strval($ipAddress);
}

function IsLocalHost($ip)
{
    return $ip == "::1";
}

function MakeConnectionToDB()
{
    return new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DBNAME);
}

function GetIPGEOData($ThisIp)
{
    $temp = json_decode(file_get_contents("http://ipinfo.io/{$ThisIp}/json"));
    $userDetail = "";
    $userDetail = property_exists($temp, 'city') ? $userDetail . ($temp->city) : $userDetail;
    $userDetail = property_exists($temp, 'region') ? $userDetail . ", " . ($temp->region) : $userDetail;
    $userDetail = property_exists($temp, 'country') ? $userDetail . ", " . ($temp->country) : $userDetail;
    return $userDetail;
}

function GetCurrentGEOData()
{
    return GetIPGEOData(Getip());
}

function Make_Query($input)
{
    $conn = MakeConnectionToDB();
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error . "\n");
    }

    $result = $conn->query($input);

    if ($result) {
//            debug_to_console( "SQL Executed Without Error\n");
    } else {
        die("Error : " . $conn->error . "\n");
    }

    $conn->close();
    return $result;
}

function HandleRecordThisAccess()
{
    HandleRecordIP(Getip());
}

function HandleRecordIP($ThisIp)
{
    $userDetail = GetIPGEOData($ThisIp);

    if (USE_IP_BLACKLIST) {
        if (in_array($ThisIp, IP_BLACKLIST)) {
            return;
        }
    }

    Make_Query("INSERT INTO MyGuests (ip,detail) VALUES ('" . $ThisIp . "', '" . $userDetail . "')");
}


function GetGuestData()
{
    $result = Make_Query("SELECT detail, count(detail) FROM " . DB_TABLE . " GROUP by detail");
    $data = array(
        array('geo', 'geo count')
    );
    while ($row = $result->fetch_assoc()) {
        $geo = $row['detail'];
        $count = (int)$row['count(detail)'];
        array_push($data, array($geo, $count));
    }

    return JSON_encode($data);
}


function StartsWith($haystack, $needle)
{
    $length = strlen($needle);
    return substr($haystack, 0, $length) === $needle;
}

function EndsWith($haystack, $needle)
{
    $length = strlen($needle);
    if (!$length) {
        return true;
    }
    return substr($haystack, -$length) === $needle;
}

function StrContains($str, $sub)
{
    if (strpos($str,$sub) !== false)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function HandleRemoveLatestRecord()
{
    HandleRemoveRecordIP(Getip());
}

function HandleRemoveRecordIP($ThisIp)
{
    Make_Query("DELETE FROM MyGuests WHERE ip='". $ThisIp . "' ORDER BY access_time DESC LIMIT 1");
}

function GetCountOfConnection()
{
    return GetCountOfRecordIP(Getip());
}

function GetCountOfRecordIP($ThisIp)
{
    $userDetail = GetIPGEOData($ThisIp);
    $callback = Make_Query("select COUNT(*) from ( select ip from MyGuests where ip = '" . $ThisIp ."') subselect");
    $result = "";
    while($row = $callback->fetch_array(MYSQLI_NUM)) {
        $result = $result . $row[0];
    }
    return $result;
}

