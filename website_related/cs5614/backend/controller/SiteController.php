<?php

include_once '../global.php';

#handle recording ip
if (!isset($_GET['ignoreIp']))
{
    if (USE_PASS_WORD)
    {
        $sc->Password();
    }else
    {
        HandleRecordThisAccess();
    }
}

$route = $_GET['route'];
$sc = new SiteController();

if ($route == 'traffic') {

    $action = $_GET['action'];
    switch ($action) {
        case "info":
            $sc->TrafficInfo();
            break;
        case "chart":
            $sc->TrafficChart();
            break;
        case "delete":
            $sc->TrafficDelete();
            break;
        default:
            //if no action is defined above
            $sc->Traffic();
            break;
    }
}
elseif ($route == 'timeline') {
    $sc->Timeline();
}elseif ($route == 'home') {
    $sc->Home();
}elseif ($route == 'demo') {
    $sc->Construction('Demo');
}elseif ($route == 'report') {
    $sc->Construction('Report');
}elseif ($route == 'analysis') {
    $sc->Analysis();
}

class SiteController
{

    public function Password()
    {
        include_once PLUGINS_PATH . '/password_protect.php';
    }

    public function Analysis()
    {
        $pageTitle = 'Analysis';
        include_once VIEWS_PATH . '/header.php';
        include_once VIEWS_PATH . '/others/analysis.php';
        include_once VIEWS_PATH . '/footer.php';
    }

    public function Timeline()
    {
        $pageTitle = 'Timeline';
        include_once VIEWS_PATH . '/header.php';
        include_once VIEWS_PATH . '/timeline/main.php';
        include_once VIEWS_PATH . '/footer.php';
    }

    public function Home()
    {
        $pageTitle = 'CS5614 Group : ' . GROUP_NAME;
        include_once VIEWS_PATH . '/header.php';
        include_once VIEWS_PATH . '/main.php';
        include_once VIEWS_PATH . '/footer.php';
    }

    public function Construction($MissingPageName)
    {
        $pageTitle = 'Nothing here in ' . $MissingPageName . ' :(';
        include_once VIEWS_PATH . '/header.php';
        include_once VIEWS_PATH . '/construction.php';
        include_once VIEWS_PATH . '/footer.php';
    }

    /**
     * Traffic related
     */
    public function Traffic()
    {
        $pageTitle = 'Traffic';
        include_once VIEWS_PATH . '/header.php';
        include_once VIEWS_PATH . '/others/traffic/main.php';
        include_once VIEWS_PATH . '/footer.php';
    }

    public function TrafficDelete()
    {
        //remove ip record
        HandleRemoveLatestRecord();
        //call parent js function to refresh info panel
        echo "<script>parent.ShowLatestInfoDate();</script>";
        //update the chart
        SiteController::TrafficChart();
    }

    public function TrafficChart()
    {
        //iframe
        include_once VIEWS_PATH . '/others/traffic/chart.php';
    }

    public function TrafficInfo()
    {
        //iframe
        include_once VIEWS_PATH . '/others/traffic/info.php';
    }



}
