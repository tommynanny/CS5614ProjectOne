<html>
<head>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="<?=JS_URL?>/jquery-slim.min.js"><\/script>')</script>
    <script src="https://riversun.github.io/jsframe/jsframe.js"></script>
    <script src="<?= JS_URL ?>/rainbowvis.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.load("visualization", "1", {packages: ["corechart"]});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var array = JSON.parse('<?=GetGuestData()?>');
            var data = google.visualization.arrayToDataTable(array);

            var options = {
                colors: Colorings(array.length, 'green', 'yellow'),
                title: 'Visit Traffic',
                is3D: true,
                pieSliceText: 'value',
                legend: {
                    position: 'labeled',
                    labeledValueText: 'both',
                    textStyle: {
                        color: 'blue',
                        fontSize: 14
                    },
                }
            };
            var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
            chart.draw(data, options);
        }

        function Colorings(count, start, end) {
            var numberOfItems = count;
            var rainbow = new Rainbow();
            rainbow.setNumberRange(1, numberOfItems);
            rainbow.setSpectrum(start, end);
            var result = [];
            for (var i = 1; i <= numberOfItems; i++) {
                var hexColour = rainbow.colourAt(i);
                result.push(hexColour);
            }
            return result;
        }

        $(window).resize(function () {
            drawChart();
        });
    </script>
    <style>
        .chart {
            min-width: 800px;
            height: 100%;
        }

        .row {
            margin: 0 !important;
        }
    </style>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
</head>
<body>
<div class="row">
    <div class="clearfix"></div>
    <div class="row">
        <div id="piechart_3d" class="chart"></div>
    </div>
</div>
</body>
</html>
