<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="<?=JS_URL?>/jquery-slim.min.js"><\/script>')</script>

<script src="<?= JS_URL ?>/rainbowvis.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
    $(document).ready(function () {

        ShowChart('<?=BASE_URL_NO_WEB?>/traffic/chart/');
        Chartframe.requestFocus();
        ShowInfo('<?=BASE_URL_NO_WEB?>/traffic/info/');
        Infoframe.requestFocus();
    });

    var avail = ['yosemite'];
    var index = Math.floor(Math.random() * avail.length);
    var current = avail[index];

    var w = 810;
    var h = 500;
    var l = ($(window).width() / 2 - w / 2);
    var t = ($(window).height() / 2 - h / 2);

    var Infoframe;
    var Chartframe;

    function ShowLatestTrafficData() {
        Chartframe.closeFrame();
        ShowChart('<?=BASE_URL_NO_WEB?>/traffic/delete/');
    }

    function ShowLatestInfoDate() {
        Infoframe.closeFrame();
        ShowInfo('<?=BASE_URL_NO_WEB?>/traffic/info/#nav');
    }

    function ShowInfo(input) {
        // window.alert(w + " " + h + " " + l + " " + " " + t);
        const jsFrame0 = new JSFrame();
        Infoframe = jsFrame0.create({
            title: "info.",
            left: (l - w / 3) >= 30 ? (l - w / 3) : 30, top: t, width: w / 2, height: h * 3 / 5,
            movable: true,//Enable to be moved by mouse
            resizable: false,//Enable to be resized by mouse
            appearanceName: 'material',
            url: input,//URL to display in iframe
            style: {
                borderRadius: '2px',
                // backgroundColor: 'rgba(0,124,255,1)',
            },
            //urlLoaded:Callback function called after loading iframe
            urlLoaded: (frame) => {
                //Called when the url finishes loading
            },
            appearanceParam: {
                border: {
                    shadow: '2px 2px 10px  rgba(0, 0, 0, 0.5)',
                    width: 0,
                    radius: 6,
                },
                titleBar: {
                    color: 'white',
                    background: '#4784d4',
                    leftMargin: 40,
                    height: 30,
                    fontSize: 14,
                    buttonWidth: 36,
                    buttonHeight: 16,
                    buttonColor: 'white',
                    buttons: [ // buttons on the right
                        {
                            //Set font-awesome fonts(https://fontawesome.com/icons?d=gallery&m=free)
                            fa: 'fas fa-times',//code of font-awesome
                            name: 'closeButton',
                            visible: true // visibility when window is created.
                        },
                    ],
                    buttonsOnLeft: [ //buttons on the left
                        {
                            //Set font-awesome fonts(https://fontawesome.com/icons?d=gallery&m=free)
                            fa: 'fas fa-exclamation-circle',
                            name: 'menu',
                            visible: true
                        },
                    ],
                },
            },
        });

        Infoframe.on('menu', 'click', (_frame, evt) => {

        });

        //Show the window
        Infoframe.show();
        Infoframe.setControl({hideButton: 'closeButton'});
    }

    function ShowChart(input) {
        const jsFrame = new JSFrame();
        Chartframe = jsFrame.create({
            title: 'Traffic Data',
            left: l + w / 4, top: t + h / 10, width: w, height: h,
            movable: true,//Enable to be moved by mouse
            resizable: false,//Enable to be resized by mouse
            appearanceName: current,
            url: input,//URL to display in iframe
            style: {
                borderRadius: '2px',
                // backgroundColor: 'rgba(0,124,255,1)',
            },
            //urlLoaded:Callback function called after loading iframe
            urlLoaded: (frame) => {
                //Called when the url finishes loading
            },
            appearanceParam: {
                showCloseButton: false,
                border: {
                    shadow: '2px 2px 10px  rgba(0, 0, 0, 0.5)',
                    width: 0,
                    radius: 6,
                },
                titleBar: {
                    color: 'white',
                    background: '#4784d4',
                    leftMargin: 40,
                    height: 30,
                    fontSize: 14,
                    buttonWidth: 36,
                    buttonHeight: 16,
                    buttonColor: 'white',
                    buttons: [ // buttons on the right
                        {
                            //Set font-awesome fonts(https://fontawesome.com/icons?d=gallery&m=free)
                            fa: 'fas fa-times',//code of font-awesome
                            name: 'closeButton',
                            visible: true // visibility when window is created.
                        },
                        {
                            fa: 'fas fa-compress-arrows-alt',
                            name: 'minimizedButton',
                            visible: true
                        },
                    ],
                    buttonsOnLeft: [ //buttons on the left
                        {
                            //Set font-awesome fonts(https://fontawesome.com/icons?d=gallery&m=free)
                            fa: 'fas fa-bars',
                            name: 'menu',
                            visible: true
                        },
                    ],
                },
            },
        });

        Chartframe.setControl({
            hideButton: 'closeButton'
        });
        Chartframe.on('menu', 'click', (_frame, evt) => {

        });

        Chartframe.on('closeButton', 'click', (_frame, evt) => {
        });

        //Show the window
        Chartframe.show();
    }
</script>


<main role="main" class="container">
    <div class="row">
        <section id="analysis" class="mt-5">
            <h3 class="display-5"><i class="fas fa-globe-americas"></i> Traffic</h3>
            <hr>

        </section>
        <div id="analysis_nav"></div>
    </div>
</main>