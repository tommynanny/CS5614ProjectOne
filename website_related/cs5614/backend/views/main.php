<!-- Begin page content -->
<main role="main" class="container">
    <div class="row">
        <div class="col-4 mx-auto d-block">
            <div class="row mt-5 mx-auto d-block text-center">
            <span>
                 <div class="form-inline row mt-5 mx-auto text-center">
                    <div class="form-group col-4 row" href="#!" onclick="SwitchPhoto(this)">
                        <img src="<?= IMG_URL ?>/Members/YuweiXie.jpg" class="img-fluid rounded-circle mb-3"
                             alt="Profile picture">
                        <h4>Yuwei Xie</h4>
                    </div>
                     <div class="form-group col-4 row" href="#!" onclick="SwitchPhoto(this)">
                        <img src="<?= IMG_URL ?>/Members/SurajGupta.jpg" class="img-fluid rounded-circle mb-3"
                             alt="Profile picture">
                        <h4>Suraj Gupta</h4>
                     </div>
                     <div class="form-group col-4 row" href="#!" onclick="SwitchPhoto(this)">
                        <img src="<?= IMG_URL ?>/Members/NanlinSun.jpg" class="img-fluid rounded-circle mb-3"
                             alt="Profile picture">
                        <h4>Nanlin Sun</h4>
                     </div>
                 </div>
            </span>
                <p class="font-weight-light">Virginia Polytechnic Institute and State University(<a
                            href="https://www.vt.edu/"><abbr
                                title="Virginia Polytechnic Institute and State University">VT</abbr></a>)<br>
                    Computer Science (<a href="http://www.cs.vt.edu/"><abbr title="Computer Science">CS</abbr></a>)</p>
            </div>
            <hr>
            <div class="row mx-auto d-block">
                <a href="mailto: yuweix@vt.edu" target="_blank"><i class="far fa-envelope"></i></a>Yuwei Xie <a
                        href="mailto: yuweix@vt.edu" target="_blank">yuweix@vt.edu</a>,<br>
                <a href="mailto: surajg4@vt.edu" target="_blank"><i class="far fa-envelope"></i></a>Suraj Gupta <a
                        href="mailto: yuweix@vt.edu" target="_blank">surajg4@vt.edu</a>,<br>
                <a href="mailto: nannie@vt.edu" target="_blank"><i class="far fa-envelope"></i></a>Nanlin Sun <a
                        href="mailto: yuweix@vt.edu" target="_blank">nannie@vt.edu</a>,<br>
                <hr>
                <p class="text-center font-weight-light">
                    <i class="far fa-flag"></i> <b><a
                                href="https://www.google.com/maps/place/Blacksburg,+VA/@37.2305147,-80.4644742,13z/data=!3m1!4b1!4m5!3m4!1s0x884d950adc06dcc3:0x86ceb8ea4842da2d!8m2!3d37.2295733!4d-80.4139393"
                                target="_blank">Blacksburg</a></b>
                    <i class="far fa-flag"></i> <b><a
                                href="https://www.google.com/maps/place/7054+Haycock+Rd,+Falls+Church,+VA+22043/@38.8967866,-77.1916669,17z/data=!3m1!4b1!4m5!3m4!1s0x89b7b4d3e8a5e66f:0xd8f88c73bc3be6db!8m2!3d38.8967866!4d-77.1894782"
                                target="_blank">Northern
                            Virginia</a></b>
                    <br><br>

                    <i class="fas fa-birthday-cake"></i> Group Since 2020<br><br>

                </p>
                <!--                <hr>-->
            </div>
        </div>
        <div class="col-8">

            <div id="studying_nav">

                <section id="studying" class="mt-5">
                    <h3 class="display-5"><i class="fas fa-graduation-cap"></i> Studying</h3>
                    <hr>
                    <h4>Course <span class="badge badge-secondary">2020 Aug-Dec</span></h4>
                    <ul class="clist">
                        <li>CS 5614 Database Management Systems <a
                                    href=""
                                    class="badge badge-info">MySQL</a> <a
                                    href="" class="badge badge-primary">Php</a>
                        </li>
                    </ul>
                </section>

                <div id="projects_nav"></div>
                <br><br>

                <section id="projects" class="mt-5">
                    <h3 class="display-5"><i class="fas fa-database"></i> Projects</h3>
                    <hr>
                    <ul class="timelinelist">
                        <li><a href="">CS5614 Project 1</a> <span
                                    class="badge badge-primary">MySQL, Php, C#</span> - Growth of different Airline
                            industries over the period of time: Visualization
                        </li>
                        <!--                    <li><a href="https://github.com/markoshorro/gem5-spm">gem5-spm</a> <span class="badge badge-info">C++, Python</span>-->
                        <!--                        - implementation of scratchpad memories in gem5 simulator-->
                        <!--                    </li>-->
                    </ul>
                </section>


            </div>

        </div>


        <script>
            function ShowSortedTable() {
                if (document.getElementById("SortedTable").style.display != "none") {
                    HideAll();
                    return;
                }
                document.getElementById("SortedAttributes").style.display = "none";
                document.getElementById("SortedTable").style.display = "block";
                document.getElementById("AttributeTableGraph").style.display = "none";
            }

            function ShowSortedAttributes() {
                if (document.getElementById("SortedAttributes").style.display != "none") {
                    HideAll();
                    return;
                }
                document.getElementById("SortedAttributes").style.display = "block";
                document.getElementById("SortedTable").style.display = "none";
                document.getElementById("AttributeTableGraph").style.display = "none";
            }

            function ShowAttributeTableGraph() {
                if (document.getElementById("AttributeTableGraph").style.display != "none") {
                    HideAll();
                    return;
                }
                document.getElementById("SortedAttributes").style.display = "none";
                document.getElementById("SortedTable").style.display = "none";
                document.getElementById("AttributeTableGraph").style.display = "block";
            }


            function HideAll() {
                document.getElementById("SortedAttributes").style.display = "none";
                document.getElementById("SortedTable").style.display = "none";
                document.getElementById("AttributeTableGraph").style.display = "none";
            }
        </script>

</main>



