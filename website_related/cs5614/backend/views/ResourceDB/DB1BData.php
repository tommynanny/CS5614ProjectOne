<?php

/**
 * BUREAU  OFTRANSPORTATIONSTATISTICS.    The  airline  origin  anddestination  survey  (db1b).
 * https://www.transtats.bts.gov/DatabaseInfo.asp?DBID=135. [Accessed: 25-Sep-2020]
 */

$DB1B = array(
    "DB1BCoupon" => "ItinID	Itinerary ID	 
MktID	Market ID	 
SeqNum	Coupon Sequence Number	 
Coupons	Number of Coupons in the Itinerary	Analysis
Year	Year	 
OriginAirportID	Origin Airport, Airport ID. An identification number assigned by US DOT to identify a unique airport. Use this field for airport analysis across a range of years because an airport can change its airport code and airport codes can be reused.	Analysis
OriginAirportSeqID	Origin Airport, Airport Sequence ID. An identification number assigned by US DOT to identify a unique airport at a given point of time. Airport attributes, such as airport name or coordinates, may change over time.	 
OriginCityMarketID	Origin Airport, City Market ID. City Market ID is an identification number assigned by US DOT to identify a city market. Use this field to consolidate airports serving the same city market.	Analysis
Quarter	Quarter (1-4)	Analysis
Origin	Origin Airport Code	Analysis
OriginCountry	Origin Airport, Country Code	 
OriginStateFips	Origin Airport, State FIPS Code	Analysis
OriginState	Origin Airport, State Code	Analysis
OriginStateName	Origin State Name	 
OriginWac	Origin Airport, World Area Code	Analysis
DestAirportID	Destination Airport, Airport ID. An identification number assigned by US DOT to identify a unique airport. Use this field for airport analysis across a range of years because an airport can change its airport code and airport codes can be reused.	Analysis
DestAirportSeqID	Destination Airport, Airport Sequence ID. An identification number assigned by US DOT to identify a unique airport at a given point of time. Airport attributes, such as airport name or coordinates, may change over time.	 
DestCityMarketID	Destination Airport, City Market ID. City Market ID is an identification number assigned by US DOT to identify a city market. Use this field to consolidate airports serving the same city market.	Analysis
Dest	Destination Airport Code	Analysis
DestCountry	Destination Airport, Country Code	 
DestStateFips	Destination Airport, State FIPS Code	Analysis
DestState	Destination Airport, State Code	Analysis
DestStateName	Destination State Name	 
DestWac	Destination Airport, World Area Code	Analysis
Break	Trip Break Code	Analysis
CouponType	Coupon Type Code	Analysis
TkCarrier	Ticketing Carrier Code	 
OpCarrier	Operating Carrier Code	 
RPCarrier	Reporting Carrier Code	Analysis
Passengers	Number of Passengers	Analysis
FareClass	Fare Class Code. Value Is Defined By Carriers And May Not Follow The Same Standard. Not Recommended For Analysis.	Analysis
Distance	Coupon Distance	 
DistanceGroup	Distance Group, in 500 Mile Intervals	Analysis
Gateway	Gateway Indicator (1=Yes)	Analysis
ItinGeoType	Itinerary Geography Type	Analysis
CouponGeoType	Coupon Geography Type	Analysis"
,


    "DB1BMarket" => "ItinID	Itinerary ID	 
MktID	Market ID	 
MktCoupons	Number of Coupons in the Market	Analysis
Year	Year	 
Quarter	Quarter (1-4)	Analysis
OriginAirportID	Origin Airport, Airport ID. An identification number assigned by US DOT to identify a unique airport. Use this field for airport analysis across a range of years because an airport can change its airport code and airport codes can be reused.	Analysis
OriginAirportSeqID	Origin Airport, Airport Sequence ID. An identification number assigned by US DOT to identify a unique airport at a given point of time. Airport attributes, such as airport name or coordinates, may change over time.	 
OriginCityMarketID	Origin Airport, City Market ID. City Market ID is an identification number assigned by US DOT to identify a city market. Use this field to consolidate airports serving the same city market.	Analysis
Origin	Origin Airport Code	Analysis
OriginCountry	Origin Airport, Country Code	 
OriginStateFips	Origin Airport, State FIPS Code	Analysis
OriginState	Origin Airport, State Code	Analysis
OriginStateName	Origin State Name	 
OriginWac	Origin Airport, World Area Code	Analysis
DestAirportID	Destination Airport, Airport ID. An identification number assigned by US DOT to identify a unique airport. Use this field for airport analysis across a range of years because an airport can change its airport code and airport codes can be reused.	Analysis
DestAirportSeqID	Destination Airport, Airport Sequence ID. An identification number assigned by US DOT to identify a unique airport at a given point of time. Airport attributes, such as airport name or coordinates, may change over time.	 
DestCityMarketID	Destination Airport, City Market ID. City Market ID is an identification number assigned by US DOT to identify a city market. Use this field to consolidate airports serving the same city market.	Analysis
Dest	Destination Airport Code	Analysis
DestCountry	Destination Airport, Country Code	 
DestStateFips	Destination Airport, State FIPS Code	Analysis
DestState	Destination Airport, State Code	Analysis
DestStateName	Destination State Name	 
DestWac	Destination Airport, World Area Code	Analysis
AirportGroup	Airport Group	 
WacGroup	World Area Code Group	 
TkCarrierChange	Ticketing Carrier Change Indicator (1=Yes)	Analysis
TkCarrierGroup	Ticketing Carrier Group	 
OpCarrierChange	Operating Carrier Change Indicator (1=Yes)	Analysis
OpCarrierGroup	Operating Carrier Group	 
RPCarrier	Reporting Carrier Code	Analysis
TkCarrier	Ticketing Carrier Code for On-line Itineraries (otherwise equal to 99)	 
OpCarrier	Operating Carrier Code for On-line Itineraries (otherwise equals to 99)	 
BulkFare	Bulk Fare Indicator (1=Yes)	 
Passengers	Number of Passengers	Analysis
MktFare	Market Fare (ItinYield*MktMilesFlown)	 
MktDistance	Market Distance (Including Ground Transport)	 
MktDistanceGroup	Distance Group, in 500 Mile Intervals	Analysis
MktMilesFlown	Market Miles Flown (Track Miles)	 
NonStopMiles	Non-Stop Market Miles (Using Radian Measure)	 
ItinGeoType	Itinerary Geography Type	Analysis
MktGeoType	Market Geography Type	Analysis"
,

    "DB1BTicket" => "ItinID	Itinerary ID	 
Coupons	Number of Coupons in the Itinerary	Analysis
Year	Year	 
Quarter	Quarter (1-4)	Analysis
Origin	Origin Airport Code	Analysis
OriginAirportID	Origin Airport, Airport ID. An identification number assigned by US DOT to identify a unique airport. Use this field for airport analysis across a range of years because an airport can change its airport code and airport codes can be reused.	Analysis
OriginAirportSeqID	Origin Airport, Airport Sequence ID. An identification number assigned by US DOT to identify a unique airport at a given point of time. Airport attributes, such as airport name or coordinates, may change over time.	 
OriginCityMarketID	Origin Airport, City Market ID. City Market ID is an identification number assigned by US DOT to identify a city market. Use this field to consolidate airports serving the same city market.	Analysis
OriginCountry	Origin Airport, Country	 
OriginStateFips	Origin Airport, State FIPS	Analysis
OriginState	Origin Airport, State	Analysis
OriginStateName	Origin State Name	 
OriginWac	Origin Airport, World Area Code	Analysis
RoundTrip	Round Trip Indicator (1=Yes)	Analysis
OnLine	Single Carrier Indicator (1=Yes)	Analysis
DollarCred	Dollar Credibility Indicator	Analysis
FarePerMile	Itinerary Fare Per Miles Flown in Dollars (ItinFare/MilesFlown).	 
RPCarrier	Reporting Carrier	Analysis
Passengers	Number of Passengers	Analysis
ItinFare	Itinerary Fare Per Person	 
BulkFare	Bulk Fare Indicator (1=Yes)	 
Distance	Itinerary Distance (Including Ground Transport)	 
DistanceGroup	Distance Group, in 500 Mile Intervals	Analysis
MilesFlown	Itinerary Miles Flown (Track Miles)	 
ItinGeoType	Itinerary Geography Type	Analysis");
?>