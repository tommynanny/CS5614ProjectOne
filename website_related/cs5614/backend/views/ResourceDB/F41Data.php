<?php

/**
 * BUREAU  OFTRANSPORTATIONSTATISTICS.    The  airline  origin  anddestination  survey  (db1b).
 * https://www.transtats.bts.gov/DatabaseInfo.asp?DBID=135. [Accessed: 25-Sep-2020]
 */

$F41D = array(
    "Schedule B-1" =>
        "//Current Assets
Cash	10100 - Cash (000)	Analysis
ShortTermInv	11000 - Short Term Investments (000)	Analysis
NotesReceivable	12000 - Notes Receivable (000)	Analysis
AcctsReceivable	12700 - Accounts Receivable (000)	Analysis
AcctsNotCollect	12900 - Less: Allowance For Uncollectible Accounts, Credit (-) (000)	Analysis
NotesAccRecNet	12990 - Notes And Accounts Receivable - Net (000)	Analysis
PartsSuppliesNet	13990 - Spare Parts And Supplies - Net (000)	Analysis
PrepaidItems	14100 - Prepaid Items (000)	Analysis
CurrAssetsOth	14200 - Other Current Assets (000)	Analysis
CurrAssets	14990 - Total Current Assets (000)	Analysis
//Investments and Special Funds
InvestAssocComp	15100 - Investments In Associated Companies (000)	Analysis
InvestRecOth	15300 - Other Investments And Receivables (000)	Analysis
SpecialFunds	15500 - Special Funds (000)	Analysis
InvestSpecFunds	15990 - Total Investments And Special Funds (000)	Analysis
//Operating Property and Equipment
FlightEquip	16090 - Flight Equipment (000)	Analysis
PropEquipGround	16490 - Ground Property And Equipment (000)	Analysis
DeprPrEqGround	16680 - Less: Allowances For Depreciation, Credit (-) (000)	Analysis
PropEquipNet	16750 - Property And Equipment - Net (000)	Analysis
Land	16790 - Land (000)	Analysis
EquipDepAdvPay	16850 - Equipment Purchase Deposits And Advance Payments (000)	Analysis
Construction	16890 - Construction Work In Progress (000)	 
LeasedPropCap	16950 - Leased Property Under Capital Leases (000)	Analysis
LeasedPropAcc	16960 - Leased Property Under Capital Leases Accumulated Amortization, Credit (-) (000)	Analysis
PropEquip	16990 - Total Operating Property And Equipment (000)	Analysis
//NonOperating Property and Equipment
PropEquipNonOp	17910 - Nonoperating Property And Equipment (000)	Analysis
DeprPrEqNonOp	17920 - Less: Allowance For Depreciation/Amortization, Credit (-) (000)	Analysis
PropEquipNOTot	17990 - Total Nonoperating Property And Equipment (000)	Analysis
//Other Assets
PrePayLongTerm	18200 - Long-Term Prepayments (000)	Analysis
NonAmortDev	18300 - Unamortized Development And Preoperating Costs (000)	Analysis
AssetsOthDef	18900 - Other Assets And Deferred Charges (000)	Analysis
AssetsOther	18950 - Total Other Assets (000)	Analysis
//Total Assets
Assets	18990 - Total Assets (000)	Analysis
//Current Liabilities
LongDebtCurMat	20000 - Current Maturities Of Long-Term Debt (000)	Analysis
NotesPayBanks	20050 - Notes Payable - Banks (000)	Analysis
NotesPayOther	20150 - Notes Payable - Others (000)	Analysis
AcctsPayTrade	20210 - Trade Accounts Payable (000)	Analysis
AcctsPayOther	20250 - Accounts Payable - Others (000)	Analysis
CurrObCapLease	20800 - Current Obligations Under Capital Leases (000)	Analysis
AccrSalaries	21100 - Accrued Salaries Wages (000)	Analysis
AccrVacation	21200 - Accrued Vacation Liability (000)	Analysis
AccrInterest	21250 - Accrued Interest (000)	Analysis
AccrTaxes	21300 - Accrued Taxes (000)	Analysis
Dividends	21400 - Dividends Declared (000)	Analysis
LiabAirTraffic	21600 - Air Traffic Liability (000)	Analysis
CurrLiabOth	21900 - Other Current Liabilities (000)	Analysis
CurrLiabilities	21990 - Total Current Liabilities (000)	Analysis
//NonCurrrent Liabilities
LongTermDebt	22100 - Long-Term Debt (000)	Analysis
AdvAssocComp	22400 - Advances From Associated Companies (000)	Analysis
PensionLiab	22500 - Pension Liability (000)	Analysis
NonRecObCapLs	22800 - Noncurrent Obligations Under Capital Leases (000)	Analysis
NonRecLiabOth	22900 - Other Noncurrent Liabilities (000)	Analysis
NonRecLiab	22990 - Total Noncurrent Liabilities (000)	Analysis
//Deferred Credits
DefTaxes	23400 - Deferred Income Taxes (000)	Analysis
DefTaxCredits	23450 - Deferred Investment Tax Credits (000)	Analysis
DefCreditsOth	23900 - Other Deferred Credits (000)	Analysis
DefCredits	23990 - Total Deferred Credits (000)	Analysis
//Stockholders Equity
PfShares	28200 - Capital Stock Preferred Shares Issued (000)	Analysis
PfSharesNum	2820A - Capital Stock Number Of Preferred Shares Issued (000)	Analysis
ComShares	28400 - Capital Stock Common Shares Issued (000)	Analysis
UnissuedStock	28600 - Capital Stock Subscribed And Unissued (000)	Analysis
CapitalStock	28690 - Total Capital Stock (000)	Analysis
AddCapitalInv	28900 - Additional Capital Invested (000)	Analysis
PaidInCapital	28990 - Total Paid-In Capital (000)	Analysis
RetEarnings	29000 - Retained Earnings (000)	Analysis
ShHldEquity	29590 - Total Stockholders' Equity (000)	Analysis
TreasStockNum	29900 - Less Treasury Stock, Credit (-) (000)	Analysis
ShHldEquitNet	29950 - Net Stockholders' Equity (000)	Analysis
//Total Liabilities and Stockholders Equity
LiabShHldEquity	29990 - Total Liabilities And Stockholders' Equity (000)	Analysis
//Carrier Information
AirlineID	An identification number assigned by US DOT to identify a unique airline (carrier). A unique airline (carrier) is defined as one holding and reporting under the same DOT certificate regardless of its Code, Name, or holding company/corporation.	Analysis
UniqueCarrier	Unique Carrier Code. When the same code has been used by multiple carriers, a numeric suffix is used for earlier users, for example, PA, PA(1), PA(2). Use this field for analysis across a range of years.	Analysis
UniqueCarrierName	Unique Carrier Name. When the same name has been used by multiple carriers, a numeric suffix is used for earlier users, for example, Air Caribbean, Air Caribbean (1).	 
Carrier	Code assigned by IATA and commonly used to identify a carrier. As the same code may have been assigned to different carriers over time, the code is not always unique. For analysis, use the Unique Carrier Code.	 
CarrierName	Carrier Name	 
UniqCarrierEntity	Unique Entity for a Carrier's Operation Region.	Analysis
CarrierRegion	Carrier's Operation Region. Carriers Report Data by Operation Region	Analysis
CarrierGroupNew	Carrier Group New	Analysis
CarrierGroup	Carrier Group Code. Used in Legacy Analysis	Analysis
//Time Period Information
Year	Year	 
Quarter	Quarter (1-4)	 "
,


    "Schedule B-1.1" =>
        "//Current Assets
Cash	00010 - Current Assets - Cash And Equivalents (000)	Analysis
NotesAccRecNet	00020 - Current Assets - Notes And Accounts Receivable - Net (000)	Analysis
CurrAssetsOth	00030 - Other Current Assets (000)	Analysis
CurrAssets	00040 - Total Current Assets (000)	Analysis
//Operating Property and Equipment
PropEquipOwned	00050 - Owned Property And Equipment (000)	Analysis
DeprPropEquip	00060 - Less : Accumulated Depreciation, Credit (-) (000)	Analysis
PropEquipCapLs	00070 - Property And Equipment Obtained Under Capital Leases (000)	Analysis
AmortPrEqCapLs	00080 - Less: Accumulated Amortization, Credit (-) (000)	 
PropEquip	00090 - Total Property And Equipment (000)	Analysis
//Other Assets
AssetsOther	00100 - Other Assets (000)	Analysis
//Total Assets
Assets	00110 - Total Assets (000)	Analysis
//Current Liabilities
NotesAccPayable	00120 - Current Liabilities - Notes And Accounts Payable (000)	Analysis
AccruedTaxes	00130 - Current Liabilities - Accrued Taxes (000)	Analysis
CurrLiabOther	00140 - Other Current Liabilities (000)	Analysis
CurrLiabilities	00150 - Total Current Liabilities (000)	Analysis
//NonCurrent Liabilities
LongTermDebt	00160 - Long - Term Debt (000)	Analysis
OthLiabilities	00170 - Other Liabilities (000)	Analysis
//Deferred Credits
DeferredCredits	00180 - Deferred Credits (000)	Analysis
//Stockholders Equity
PfEquity	00190 - Capital Stock - Preferred - Equity (000)	Analysis
PfSharesNum	0019A - Capital Stock - Preferred - Number Of Shares Outstanding (000)	Analysis
ComEquity	00200 - Capital Stock - Common - Equity (000)	Analysis
ComSharesNum	0020A - Capital Stock - Common - Number Of Shares Outstanding (000)	Analysis
PaidInCapital	00210 - Other Paid In Capital (000)	Analysis
RetEarnings	00220 - Retained Earnings (000)	Analysis
ShHldEquity	00230 - Total Stockholders' Equity (000)	Analysis
TreasStock	00240 - Less: Treasury Stock, Credit (-) (000)	Analysis
ShHldEquityNet	00250 - Net Stockholders' Equity (000)	Analysis
//Total Liabilities and Stockholders' Equity
LiabShHldEquity	00260 - Total Liabilities And Stockholders' Equity (000)	Analysis
//Carrier Information
AirlineID	An identification number assigned by US DOT to identify a unique airline (carrier). A unique airline (carrier) is defined as one holding and reporting under the same DOT certificate regardless of its Code, Name, or holding company/corporation.	Analysis
UniqueCarrier	Unique Carrier Code. When the same code has been used by multiple carriers, a numeric suffix is used for earlier users, for example, PA, PA(1), PA(2). Use this field for analysis across a range of years.	Analysis
UniqueCarrierName	Unique Carrier Name. When the same name has been used by multiple carriers, a numeric suffix is used for earlier users, for example, Air Caribbean, Air Caribbean (1).	 
Carrier	Code assigned by IATA and commonly used to identify a carrier. As the same code may have been assigned to different carriers over time, the code is not always unique. For analysis, use the Unique Carrier Code.	 
CarrierName	Carrier Name	 
UniqCarrierEntity	Unique Entity for a Carrier's Operation Region.	Analysis
CarrierRegion	Carrier's Operation Region. Carriers Report Data by Operation Region	Analysis
CarrierGroupNew	Carrier Group New	Analysis
CarrierGroup	Carrier Group Code. Used in Legacy Analysis	Analysis
//Time Period Information
Year	Year	 
Quarter	Quarter (Semi-Annual)	 "
,


    "Schedule B-43 Inventory" =>
        "Year	Year	 
Carrier	Code assigned by IATA and commonly used to identify a carrier. As the same code may have been assigned to different carriers over time, the code is not always unique. For analysis, use the Unique Carrier Code.	 
CarrierName	Reporting Carrier Name	 
UniqueCarrierName	Unique Carrier Name. When the same name has been used by multiple carriers, a numeric suffix is used for earlier users, for example, Air Caribbean, Air Caribbean (1).	 
ManufactureYear	Manufacture Year	 
SerialNumber	Serial Number	 
TailNumber	Tail Number	 
AircraftStatus	Aircraft Status	Analysis
OperatingStatus	Operating Status	Analysis
NumberOfSeats	Number Of Seats	Analysis
Manufacturer	Manufacturer	 
Model	Type, Model and Cabin Design	 
CapacityInPounds	Available Capacity in Pounds	 
AcquisitionDate	Date Acquired or Placed in Service	 
AirlineID	An identification number assigned by US DOT to identify a unique airline (carrier). A unique airline (carrier) is defined as one holding and reporting under the same DOT certificate regardless of its Code, Name, or holding company/corporation.	Analysis
UniqueCarrier	Unique Carrier Code. When the same code has been used by multiple carriers, a numeric suffix is used for earlier users, for example, PA, PA(1), PA(2). Use this field for analysis across a range of years.	Analysis"
,


    "Schedule P-1(a) Employees" =>

        "Year	Year	 
Month	Month	Analysis
AirlineID	An identification number assigned by US DOT to identify a unique airline (carrier). A unique airline (carrier) is defined as one holding and reporting under the same DOT certificate regardless of its Code, Name, or holding company/corporation.	Analysis
UniqueCarrier	Unique Carrier Code. When the same code has been used by multiple carriers, a numeric suffix is used for earlier users, for example, PA, PA(1), PA(2). Use this field for analysis across a range of years.	Analysis
UniqueCarrierName	Unique Carrier Name. When the same name has been used by multiple carriers, a numeric suffix is used for earlier users, for example, Air Caribbean, Air Caribbean (1).	 
Carrier	Code assigned by IATA and commonly used to identify a carrier. As the same code may have been assigned to different carriers over time, the code is not always unique. For analysis, use the Unique Carrier Code.	 
CarrierName	Reporting Carrier Name	 
CarrierGroup	Carrier Group Code. Used in Legacy Analysis	Analysis
CarrierGroupNew	Carrier Group New	Analysis
FullTimeEmployees	Monthly Number of Full-Time Employees (For Filter Statistics = 'Sum' for yearly data, the results are a Monthly Average)	Analysis
PartTimeEmployees	Monthly Number of Part-Time Employees (For Filter Statistics = 'Sum' for yearly data, the results are a Monthly Average)	Analysis
TotalEmployees	Monthly Number of Full-Time and Part-Time Employees (For Filter Statistics = 'Sum' for yearly data, the results are a Monthly Average)	Analysis
FTEEmployees	Monthly Number of FTE's (Full-Time Equivalent Employees count two part-time employees as one full-time employee) (For Filter Statistics = 'Sum' for yearly data, the results are a Monthly Average)	Analysis"
,

    "Schedule P-1.1" =>
        "//Net Income, Profit or Loss
NetIncome	00190 - Net Income, Profit or Loss (-) (000)	Analysis
//Operating Profit or Loss
OpProfit	00140 - Operating Profit or Loss (-) (000)	Analysis
//Operating Revenues
OpRevenueSchPax	00010 - Scheduled Service - Passenger (000)	Analysis
OpRevenueSchOth	00020 - Scheduled Service - Other (000)	Analysis
OpRevenueNonSch	00030 - Nonscheduled Service (000)	Analysis
OpRevenuePubSvc	00040 - Transport Related Public Service Revenue (000)	Analysis
OpRevenueOther	00050 - Transport Related Other Revenue (000)	Analysis
OpRevenue	00060 - Total Operating Revenue (000)	Analysis
//Operating Expenses
OpExpenseFlying	00070 - Flying Operations (000)	Analysis
OpExpenseMaint	00080 - Maintenance (000)	Analysis
OpExpenseAdmin	00090 - General And Administrative (000)	Analysis
DeprPrEqOwned	00100 - Depreciation And Amortization - Owned Property And Equipment (000)	Analysis
DeprPrEqLeased	00110 - Depreciation And Amortization - Leased Property And Equipment (000)	Analysis
TransExpense	00120 - Transport Related Expense (000)	Analysis
OpExpense	00130 - Total Operating Expense (000)	Analysis
//NonOperating Income and Expenses
NonOpIntExp	00150 - Nonoperating Interest Expense, Income/Expense (-) (000)	Analysis
NonOpExpOth	00160 - Other Nonoperating - Net, Income/Expense (-) (000)	Analysis
//Income Taxes For Current Period
Tax	00170 - Income Tax, Credit (-) (000)	Analysis
//Discontinued Ops., Extraordinary Items, or Accounting Changes
ExtraItems	00180 - Discontinued Ops., Extraordinary Items, and Accounting Changes, Income/Expense (-) (000)	Analysis
//Carrier Information
AirlineID	An identification number assigned by US DOT to identify a unique airline (carrier). A unique airline (carrier) is defined as one holding and reporting under the same DOT certificate regardless of its Code, Name, or holding company/corporation.	Analysis
UniqueCarrier	Unique Carrier Code. When the same code has been used by multiple carriers, a numeric suffix is used for earlier users, for example, PA, PA(1), PA(2). Use this field for analysis across a range of years.	Analysis
UniqueCarrierName	Unique Carrier Name. When the same name has been used by multiple carriers, a numeric suffix is used for earlier users, for example, Air Caribbean, Air Caribbean (1).	 
Carrier	Code assigned by IATA and commonly used to identify a carrier. As the same code may have been assigned to different carriers over time, the code is not always unique. For analysis, use the Unique Carrier Code.	 
CarrierName	Carrier Name	 
UniqCarrierEntity	Unique Entity for a Carrier's Operation Region.	Analysis
CarrierRegion	Carrier's Operation Region. Carriers Report Data by Operation Region	Analysis
CarrierGroupNew	Carrier Group New	Analysis
CarrierGroup	Carrier Group Code. Used in Legacy Analysis	Analysis
//Time Period Information
Year	Year	 
Quarter	Quarter (1-4)	Analysis",


    "Schedule P-1.2" =>
        "//Net Income
NetIncome	98990 - Net Income, Profit or Loss (-) (000)	Analysis
//Operating Profit or Loss
OpProfitLoss	79990 - Operating Profit or Loss (-) (000)	Analysis
//Operating Revenues
TransRevPax	39010 - Transport Revenues - Scheduled Passenger (000)	Analysis
Mail	39050 - Mail (000)	Analysis
TotalProperty	39060 - Property - Freight And Passenger Baggage (000)	Analysis
PropFreight	39061 - Property - Freight (000)	Analysis
PropBag	39062 - Property - Passenger Baggage Fees (000)	Analysis
TotalCharter	39070 - Charter - Passenger And Property (000)	Analysis
CharterPax	39071 - Charter - Passenger (000)	Analysis
CharterProp	39072 - Charter - Property (000)	Analysis
TotalMiscRev	39190 - Reservation Cancellation Fees And Misc Operating Revenues (000)	Analysis
ResCancelFees	39191 - Reservation Cancellation Fees (000)	Analysis
MiscOpRev	39192 - Miscellaneous Operating Revenues (000)	Analysis
PubSvcRevenue	48080 - Public Service Revenues Subsidy (-) (000)	Analysis
TransRevenue	48980 - Transport Related Revenues (000)	Analysis
OpRevenues	49990 - Total Operating Revenues (000)	Analysis
//Operating Expenses
FlyingOps	51000 - Flying Operations (000)	Analysis
Maintenance	54000 - Maintenance (000)	Analysis
PaxService	55000 - Passenger Service (000)	Analysis
AircftServices	64000 - Aircraft And Traffic Servicing (000)	Analysis
PromotionSales	67000 - Promotion And Sales (000)	Analysis
GeneralAdmin	68000 - General And Administrative (000)	Analysis
GeneralServices	69000 - General And Administrative (000)	Analysis
DeprecAmort	70000 - Depreciation And Amortization (000)	Analysis
TransExpenses	71000 - Transport Related Expenses (000)	Analysis
OpExpenses	71990 - Total Operating Expenses (000)	Analysis
//NonOperating Income and Expenses
InterestLongDebt	81810 - Interest On Long-Term Debt And Capital Leases, Income/Expense (-) (000)	Analysis
InterestExpOth	81820 - Other Interest Expense, Income/Expense (-) (000)	Analysis
ForeignExGains	81850 - Foreign Exchange Gains And Losses, Income/Expense (-) (000)	Analysis
CapGainsProp	81885 - Capital Gains And Losses, Income/Expense (-) (000)	Analysis
CapGainsOther	81886 - Capital Gains And Losses, Income/Expense (-) (000)	 
OtherIncomeNet	81890 - Other Income And Expenses - Net, Income/Expense (-) (000)	Analysis
NonOpIncome	81990 - Nonoperating Income And Expense, Income/Expense (-) (000)	Analysis
//Income Before Income Tax
IncomePreTax	89990 - Income Before Income Tax, Profit or Loss (-) (000)	Analysis
//Income Taxes For Current Period
IncomeTax	91000 - Income Taxes For Current Period, Credit (-) (000)	Analysis
IncomeBeforeOth	91990 - Income Before Accounts 9600, 9796, 9797, And 9800, Profit or Loss (-) (000)	Analysis
//Discontinued Operations and Extraordinary Items
DiscontOps	96000 - Discontinued Operations, Income/Expense (-) (000)	Analysis
ExtraItems	97960 - Extraordinary Items, Income/Expense (-) (000)	Analysis
IncomeTaxExtra	97970 - Income Taxes Applicable To Extraordinary Items, Credit (-) (000)	Analysis
IncomeTaxCredits	97990 - Income Before Accounting Changes, Profit or Loss (-) (000)	Analysis
//Accounting Changes
AcctgChanges	98000 - Accounting Changes, Income/Expense (-) (000)	Analysis
//Carrier Information
AirlineID	An identification number assigned by US DOT to identify a unique airline (carrier). A unique airline (carrier) is defined as one holding and reporting under the same DOT certificate regardless of its Code, Name, or holding company/corporation.	Analysis
UniqueCarrier	Unique Carrier Code. When the same code has been used by multiple carriers, a numeric suffix is used for earlier users, for example, PA, PA(1), PA(2). Use this field for analysis across a range of years.	Analysis
UniqueCarrierName	Unique Carrier Name. When the same name has been used by multiple carriers, a numeric suffix is used for earlier users, for example, Air Caribbean, Air Caribbean (1).	 
Carrier	Code assigned by IATA and commonly used to identify a carrier. As the same code may have been assigned to different carriers over time, the code is not always unique. For analysis, use the Unique Carrier Code.	 
CarrierName	Carrier Name	 
UniqCarrierEntity	Unique Entity for a Carrier's Operation Region.	Analysis
CarrierRegion	Carrier's Operation Region. Carriers Report Data by Operation Region	Analysis
CarrierGroupNew	Carrier Group New	Analysis
CarrierGroup	Carrier Group Code. Used in Legacy Analysis	Analysis
//Time Period Information
Year	Year	 
Quarter	Quarter (1-4)	Analysis"
,


    "Schedule P-10" =>

        "Year	Year	 
AirlineID	An identification number assigned by US DOT to identify a unique airline (carrier). A unique airline (carrier) is defined as one holding and reporting under the same DOT certificate regardless of its Code, Name, or holding company/corporation.	Analysis
UniqueCarrier	Unique Carrier Code. When the same code has been used by multiple carriers, a numeric suffix is used for earlier users, for example, PA, PA(1), PA(2). Use this field for analysis across a range of years.	Analysis
UniqueCarrierName	Unique Carrier Name. When the same name has been used by multiple carriers, a numeric suffix is used for earlier users, for example, Air Caribbean, Air Caribbean (1).	 
CarrierCode	Code assigned by IATA and commonly used to identify a carrier. As the same code may have been assigned to different carriers over time, the code is not always unique. For analysis, use the Unique Carrier Code.	 
CarrierName	Carrier Name	 
Entity	Carrier's Operation Region. Carriers Report Data by Operation Region	Analysis
GeneralManagers	General Managers	Analysis
PilotsCopilots	Pilots and Copilots	Analysis
OtherFlightPersonnel	Other Flight Personnel	Analysis
PaxGSA	Passenger/General Services & Administration	Analysis
Maintenance	Maintenance Employees	Analysis
AcftTrafficHandlingGrp1	Aircraft Traffic Handling Group1 Employees	Analysis
GeneralAcftTrafficHandling	General Aircraft Traffic Handling Employees	Analysis
AircraftControl	Aircraft Control Employees	Analysis
PaxHandling	Passenger Handling Employees	Analysis
CargoHandling	Cargo Handling Employees	Analysis
TraineesInstructor	Trainees and Instructor	Analysis
Statistical	Statistical Employees	Analysis
TrafficSoliciters	Traffic Soliciters	Analysis
Other	Other Employees	Analysis
TransportRelated	Transport Related Employees	Analysis
Total	Total Employees	Analysis"
,

    "Schedule P-12(a)" =>
        "//Time Period Information
Year	Year	 
Quarter	Quarter (1-4)	Analysis
Month	Month	Analysis
//Carrier Information
AirlineID	An identification number assigned by US DOT to identify a unique airline (carrier). A unique airline (carrier) is defined as one holding and reporting under the same DOT certificate regardless of its Code, Name, or holding company/corporation.	Analysis
UniqueCarrier	Unique Carrier Code. When the same code has been used by multiple carriers, a numeric suffix is used for earlier users, for example, PA, PA(1), PA(2). Use this field for analysis across a range of years.	Analysis
Carrier	Code assigned by IATA and commonly used to identify a carrier. As the same code may have been assigned to different carriers over time, the code is not always unique. For analysis, use the Unique Carrier Code.	 
CarrierName	Carrier Name	 
CarrierGroupNew	Carrier Group New	Analysis
//Fuel Consumption: Scheduled Service: Domestic
SALAGallons	Scheduled Service Domestic Intra Alaska - Fuel Consumption (Gallons)	Analysis
SDOMGallons	Scheduled Service Domestic Non-Alaskan - Fuel Consumption (Gallons)	Analysis
STDOMGallons	Total Scheduled Domestic, Fuel Consumption (Gallons)	Analysis
//Fuel Consumption: Scheduled Service: International
SATLGallons	Scheduled Service International Atlantic - Fuel Consumption (Gallons)	Analysis
SPACGallons	Scheduled Service International Pacific - Fuel Consumption (Gallons)	Analysis
SLATGallons	Scheduled Service International Latin America - Fuel Consumption (Gallons)	Analysis
SINTGallons	Scheduled Service International Subtotal - Fuel Consumption (Gallons)	Analysis
//Fuel Consumption: Scheduled Service: Total
TSGallons	Total Scheduled Service - Fuel Consumption (Gallons)	Analysis
//Fuel Consumption: Non Scheduled Service: Domestic
NALAGallons	Non Scheduled Service Domestic Intra Alaska - Fuel Consumption (Gallons)	Analysis
NDOMGallons	Non Scheduled Service Domestic Non-Alaskan - Fuel Consumption (Gallons)	Analysis
NTDOMGallons	Total Non-Scheduled Domestic, Fuel Consumption (Gallons)	Analysis
//Fuel Consumption: Non Scheduled Service: International
NATLGallons	Non Scheduled Service International Atlantic - Fuel Consumption (Gallons)	Analysis
NPACGallons	Non Scheduled Service International Pacific - Fuel Consumption (Gallons)	Analysis
NLATGallons	Non Scheduled Service International Latin America - Fuel Consumption (Gallons)	Analysis
MACGallons	Non Scheduled Service International MAC - Military Airlift Command Operations (Includes Passenger/Cargo Military Contracted Flights) - Fuel Consumption (Gallons)	Analysis
NINTGallons	Non Scheduled Service International Subtotal - Fuel Consumption (Gallons)	Analysis
//Fuel Consumption: Non Scheduled Service: Total
TNGallons	Total Non Scheduled Service - Fuel Consumption (Gallons)	Analysis
//Fuel Consumption: Totals
TDOMTGallons	Total Domestic - Fuel Consumption (Gallons)	Analysis
TINTGallons	Total International - Fuel Consumption (Gallons)	Analysis
TotalGallons	Grand Total - Fuel Consumption (Gallons)	Analysis
//Fuel Cost: Scheduled Service: Domestic
SALACost	Scheduled Service Domestic Intra Alaska - Fuel Cost (Dollars)	Analysis
SDOMCost	Scheduled Service Domestic Non-Alaskan - Fuel Cost (Dollars)	Analysis
STDOMCost	Total Scheduled Domestic, Fuel Cost (Dollars)	Analysis
//Fuel Cost: Scheduled Service: International
SATLCost	Scheduled Service International Atlantic - Fuel Cost (Dollars)	Analysis
SPACCost	Scheduled Service International Pacific - Fuel Cost (Dollars)	Analysis
SLATCost	Scheduled Service International Latin America - Fuel Cost (Dollars)	Analysis
SINTCost	Scheduled Service International Subtotal - Fuel Cost (Dollars)	Analysis
//Fuel Cost: Scheduled Service: Total
TSCost	Total Scheduled Service - Fuel Cost (Dollars)	Analysis
//Fuel Cost: Non Scheduled Service: Domestic
NALACost	Non Scheduled Service Domestic Intra Alaska - Fuel Cost (Dollars)	Analysis
NDOMCost	Non Scheduled Service Domestic Non-Alaskan - Fuel Cost (Dollars)	Analysis
NTDOMCost	Total Non-Scheduled Domestic, Fuel Cost (Dollars)	Analysis
//Fuel Cost: Non Scheduled Service: International
NATLCost	Non Scheduled Service International Atlantic - Fuel Cost (Dollars)	Analysis
NPACCost	Non Scheduled Service International Pacific - Fuel Cost (Dollars)	Analysis
NLATCost	Non Scheduled Service International Latin America - Fuel Cost (Dollars)	Analysis
MACCost	Non Scheduled Service International MAC - Military Airlift Command Operations (Includes Passenger/Cargo Military Contracted Flights) - Fuel Cost (Dollars)	Analysis
NINTCost	Non Scheduled Service International Subtotal - Fuel Cost (Dollars)	Analysis
//Fuel Cost: Non Scheduled Service: Total
TNCost	Total Non Scheduled Service - Fuel Cost (Dollars)	Analysis
//Fuel Cost: Totals
TDOMTCost	Total Domestic - Fuel Cost (Dollars)	Analysis
TINTCost	Total International - Fuel Cost (Dollars)	Analysis
TotalCost	Grand Total - Fuel Cost (Dollars)	Analysis"
,

    "Schedule P-5.1" =>
        "//Flying Operations - Less Rentals
Pilotexpense	00030 - Flying Operations - Pilot And Copilot (000)	Analysis
AircraftFuel	00040 - Flying Operations - Aircraft Fuel And Oil (000)	Analysis
OtherExpenses	00050 - Flying Operations - Other (000)	Analysis
TotalDirectLessRent	00060 - Total Flying Operations - Less Rentals (000)	Analysis
//Maintenance
Maintenance	00070 - Maintenance - Flight Equipment (000)	Analysis
//Depreciation And Rental
DepandRental	00080 - Depreciation And Rental - Flight Equipment (000)	Analysis
//Direct Expense (Aircraft Operating Expense )
TotalDirect	00090 - Total Direct Expense (Aircraft Operating Expense) (000)	Analysis
//Indirect Expense
FlightAttendants	00110 - Flight Attendant Expense (000)	Analysis
Traffic	00120 - Traffic Related Expense (000)	Analysis
Departure	00130 - Departure Related Expense (Station) (000)	Analysis
Capacity	00140 - Capacity Related Expense (000)	Analysis
TotalIndirect	00150 - Total Indirect Expense (000)	Analysis
//Total Operating Expense
TotalOpExpense	00160 - Total Operating Expense (000)	Analysis
//Traffic Elements
TotalAirHours	Z6500 - Total Aircraft Airborne Hours (000) - The time the aircraft leaves the ground until the time it touches the ground again.	Analysis
AirDaysAssign	Z8100 - Aircraft Days Assigned (000) - The number of days the aircraft (owned or leased) is available for service.	Analysis
AirFuelIssued	Z9210 - Aircraft Fuel Issued in gallons (000) - The amount of fuel issued during the reporting period for revenue and non-revenue flights.	Analysis
//Aircraft Information
AircraftConfig	Aircraft Configuration	Analysis
AircraftGroup	Aircraft Group	Analysis
AircraftType	Aircraft Type	Analysis
//Carrier Information
AirlineID	An identification number assigned by US DOT to identify a unique airline (carrier). A unique airline (carrier) is defined as one holding and reporting under the same DOT certificate regardless of its Code, Name, or holding company/corporation.	Analysis
UniqueCarrier	Unique Carrier Code. When the same code has been used by multiple carriers, a numeric suffix is used for earlier users, for example, PA, PA(1), PA(2). Use this field for analysis across a range of years.	Analysis
UniqueCarrierName	Unique Carrier Name. When the same name has been used by multiple carriers, a numeric suffix is used for earlier users, for example, Air Caribbean, Air Caribbean (1).	 
Carrier	Code assigned by IATA and commonly used to identify a carrier. As the same code may have been assigned to different carriers over time, the code is not always unique. For analysis, use the Unique Carrier Code.	 
CarrierName	Carrier Name	 
UniqCarrierEntity	Unique Entity for a Carrier's Operation Region.	Analysis
CarrierRegion	Carrier's Operation Region. Carriers Report Data by Operation Region	 
CarrierGroupNew	Carrier Group New	Analysis
CarrierGroup	Carrier Group Code. Used in Legacy Analysis	Analysis
//Time Period Information
Year	Year	 
Quarter	Quarter (1-4)	Analysis"
,

    "Schedule P-5.2" =>
        "//Flying Operations
PilotFlyOps	51230 - Pilots And Copilots (000)	Analysis
OthFltFlyOps	51240 - Other Flight Personnel (000)	Analysis
TrainFlyOps	51281 - Trainees And Instructors (000)	Analysis
PersExpFlyOps	51360 - Personnel Expenses (000)	Analysis
ProFlyOps	51410 - Professional And Technical Fees And Expenses (000)	Analysis
InterchgFlyOps	51437 - Aircraft Interchange Charges (000)	Analysis
FuelFlyOps	51451 - Aircraft Fuel (000)	Analysis
OilFlyOps	51452 - Aircraft Oil (000)	Analysis
RentalFlyOps	51470 - Rentals (000)	Analysis
OtherFlyOps	51530 - Other Supplies (000)	Analysis
InsFlyOps	51551 - Insurance Purchased - General (000)	Analysis
BenefitsFlyOps	51570 - Employee Benefits And Pensions (000)	Analysis
IncidentFlyOps	51580 - Injuries, Loss, And Damage (000)	Analysis
PayTaxFlyOps	51680 - Taxes - Payroll (000)	Analysis
OthTaxFlyOps	51690 - Taxes - Other Than Payroll (000)	Analysis
OtherExpFlyOps	51710 - Other Expenses (000)	Analysis
TotFlyOps	51990 - Total Flying Operations (000)	Analysis
//Direct Maintenance - Flight Equipment
AirframeLabor	52251 - Labor - Airframes (000)	Analysis
EngineLabor	52252 - Labor - Aircraft Engines (000)	Analysis
AirframeRepair	52431 - Airframe Repairs (000)	Analysis
EngineRepairs	52432 - Aircraft Engine Repairs (000)	Analysis
InterchgCharg	52437 - Aircraft Interchange Charges (000)	Analysis
AirframeMaterials	52461 - Materials - Airframes (000)	Analysis
EngineMaterials	52462 - Materials - Aircraft Engines (000)	Analysis
AirframeAllow	52721 - Airworthiness Allowance Provisions - Airframes (000)	Analysis
AirframeOverhauls	52723 - Airframe Overhauls Deferred, Credit (-) (000)	Analysis
EngineAllow	52726 - Airworthiness Allowance Provisions - Aircraft Engines (000)	Analysis
EngineOverhauls	52728 - Aircraft Engine Overhauls Deferred, Credit (-) (000)	Analysis
TotDirMaint	52780 - Total Direct Maintenance - Flight Equipment (000)	Analysis
//Applied Maintenance Burden - Flight Equipment
ApMtBurden	52796 - Applied Maintenance Burden - Flight Equipment (000)	Analysis
TotFltMaintMemo	52990 - Total Flight Equipment Maintenance (000)	Analysis
//Net Obsolescence And Deterioration - Expendable Parts
NetObsolParts	70739 - Net Obsolescence And Deterioration - Expendable Parts (000)	Analysis
//Depreciation - Flight Equipment
AirframeDep	70751 - Airframes (000)	Analysis
EngineDep	70752 - Aircraft Engines (000)	Analysis
PartsDep	70753 - Airframe Parts (000)	Analysis
EngPartsDep	70754 - Aircraft Engine Parts (000)	Analysis
OthFltEquipDep	70755 - Other Flight Equipment (000)	Analysis
OthFltEquipDepGrpI	70756 - Total Depreciation (7075.1 - 7075.5) (000)	Analysis
//Amortization Flight Equipment
FltEquipAExp	70761 - Amortization Expense - Capital Leases - Flight Equipment (000)	Analysis
//Expense Of Interchange Aircraft
FlyOpsExpIA	70981 - Flying Operations (000)	Analysis
//Aircraft Operating Expense (Direct Operating Expense)
TotAirOpExpenses	70989 - Total Aircraft Operating Expense (Direct Operating Expense) (000)	Analysis
//Other Depreciation and Amortization (Non-Flight Equipment)
DevNPreopExp	70741 - Amortization - Developmental And Preop. Expense (000)	Analysis
OthIntangibles	70742 - Amortization - Other Intangibles (000)	Analysis
EquipNHangarDep	70758 - Depreciation - Maintenance Equipment And Hangars (000)	Analysis
GPropDep	70759 - Depreciation - General Ground Property (000)	Analysis
CapLeasesDep	70762 - Amortization - Capital Leases (000)	Analysis
//Traffic Elements
TotalAirHours	Z6500 - Total Aircraft Airborne Hours (000) - The time the aircraft leaves the ground until the time it touches the ground again.	Analysis
AirDaysAssign	Z8100 - Aircraft Days Assigned (000) - The number of days the aircraft (owned or leased) is available for service.	Analysis
AirFuelIssued	Z9210 - Aircraft Fuel Issued in gallons (000) - The amount of fuel issued during the reporting period for revenue and non-revenue flights.	Analysis
//Aircraft Information
AircraftConfig	Aircraft Configuration	Analysis
AircraftGroup	Aircraft Group	Analysis
AircraftType	Aircraft Type	Analysis
//Carrier Information
AirlineID	An identification number assigned by US DOT to identify a unique airline (carrier). A unique airline (carrier) is defined as one holding and reporting under the same DOT certificate regardless of its Code, Name, or holding company/corporation.	Analysis
UniqueCarrier	Unique Carrier Code. When the same code has been used by multiple carriers, a numeric suffix is used for earlier users, for example, PA, PA(1), PA(2). Use this field for analysis across a range of years.	Analysis
UniqueCarrierName	Unique Carrier Name. When the same name has been used by multiple carriers, a numeric suffix is used for earlier users, for example, Air Caribbean, Air Caribbean (1).	 
Carrier	Code assigned by IATA and commonly used to identify a carrier. As the same code may have been assigned to different carriers over time, the code is not always unique. For analysis, use the Unique Carrier Code.	 
CarrierName	Carrier Name	 
UniqCarrierEntity	Unique Entity for a Carrier's Operation Region.	Analysis
CarrierRegion	Carrier's Operation Region. Carriers Report Data by Operation Region	 
CarrierGroupNew	Carrier Group New	Analysis
CarrierGroup	Carrier Group Code. Used in Legacy Analysis	Analysis
//Time Period Information
Year	Year	 
Quarter	Quarter (1-4)	Analysis"
,


    "Schedule P-6" =>
        "//Salaries
SalariesMgt	00030 - General Management Personnel (000)	Analysis
SalariesFlight	00040 - Flight Personnel (000)	Analysis
SalariesMaint	00050 - Maintenance Labor (000)	Analysis
SalariesTraffic	00060 - Aircraft And Traffic Handling Personnel (000)	Analysis
SalariesOther	00070 - Other Personnel (000)	Analysis
Salaries	00080 - Total Salaries (000)	Analysis
//Related Fringe Benefits
BenefitsPersonnel	00100 - Personnel Expense (000)	Analysis
BenefitsPensions	00110 - Employee Benefits And Pensions (000)	Analysis
BenefitsPayroll	00120 - Payroll Taxes (000)	Analysis
Benefits	00130 - Total Related Fringe Benefits (000)	Analysis
//Total Salaries And Related Fringe Benefits
SalariesBenefits	00140 - Total Salaries And Related Fringe Benefits (000)	Analysis
//Materials Purchased
AircraftFuel	00160 - Aircraft Fuel And Oil (000)	Analysis
MaintMaterial	00170 - Maintenance Material (000)	Analysis
Food	00180 - Passenger Food (000)	Analysis
OtherMaterials	00190 - Other Materials (000)	Analysis
MaterialsTotal	00200 - Total Materials (000)	Analysis
//Services Purchased
Advertising	00220 - Advertising And Other Promotions (000)	Analysis
Communication	00230 - Communication (000)	Analysis
Insurance	00240 - Insurance (000)	Analysis
OutsideEquip	00250 - Outside Flight Equipment Maintenance (000)	Analysis
CommisionsPax	00260 - Traffic Commissions - Passenger (000)	Analysis
CommisionsCargo	00270 - Traffic Commissions - Cargo (000)	Analysis
OtherServices	00280 - Other Services (000)	Analysis
ServicesTotal	00290 - Total Services (000)	Analysis
//Landing Fees
LandingFees	00300 - Landing Fees (000)	Analysis
//Rentals
Rentals	00310 - Rentals (000)	Analysis
//Depreciation
Depreciation	00320 - Depreciation (000)	Analysis
//Amortization
Amortization	00330 - Amortization (000)	Analysis
//Other
Other	00340 - Other (000)	Analysis
//Transport Related Expense
TransExpense	00350 - Transport Related Expense (000)	Analysis
//Total Operating Expense
OpExpense	00360 - Total Operating Expenses (000)	Analysis
//Carrier Information
AirlineID	An identification number assigned by US DOT to identify a unique airline (carrier). A unique airline (carrier) is defined as one holding and reporting under the same DOT certificate regardless of its Code, Name, or holding company/corporation.	Analysis
UniqueCarrier	Unique Carrier Code. When the same code has been used by multiple carriers, a numeric suffix is used for earlier users, for example, PA, PA(1), PA(2). Use this field for analysis across a range of years.	Analysis
UniqueCarrierName	Unique Carrier Name. When the same name has been used by multiple carriers, a numeric suffix is used for earlier users, for example, Air Caribbean, Air Caribbean (1).	 
Carrier	Code assigned by IATA and commonly used to identify a carrier. As the same code may have been assigned to different carriers over time, the code is not always unique. For analysis, use the Unique Carrier Code.	 
CarrierName	Carrier Name	 
UniqCarrierEntity	Unique Entity for a Carrier's Operation Region.	Analysis
CarrierRegion	Carrier's Operation Region. Carriers Report Data by Operation Region	Analysis
CarrierGroupNew	Carrier Group New	Analysis
CarrierGroup	Carrier Group Code. Used in Legacy Analysis	Analysis
//Time Period Information
Year	Year	 
Quarter	Quarter (1-4)	Analysis"
,

    "Schedule P-7" =>
        "//Direct Operating Expense - Aircraft Operating Expense
AirOpExpense	00020 - Total Aircraft Operating Expense (Direct Operating Expense) (000)	Analysis
//Indirect Operating Expense - Passenger Service Expense
FlAttExpense	00050 - Flight Attendant (000)	Analysis
FoodExpense	00060 - Food (000)	Analysis
OthInFlExpense	00070 - Other Inflight (000)	Analysis
PaxSvcExpense	00080 - Total Passenger Service Expense (000)	Analysis
//Indirect Operating Expense - Aircraft Servicing Expense
LineSvcExpense	00100 - Line Servicing (000)	Analysis
ControlExpense	00110 - Control (000)	Analysis
LandingFees	00120 - Landing Fees (000)	Analysis
AirSvcExpense	00130 - Total Aircraft Servicing Expense (000)	Analysis
//Indirect Operating Expense - Traffic Servicing Expense
TrafficExpPax	00150 - Directly Assignable To Passenger (000)	Analysis
TrafficExpCargo	00160 - Directly Assignable To Baggage And Cargo (000)	Analysis
TrafficExpOth	00170 - Not Directly Assignable (000)	Analysis
TrafficExpense	00180 - Total Traffic Servicing Expense (000)	Analysis
//Indirect Operating Expense - Reservation And Sales Expense
ResExpPax	00200 - Directly Assignable To Passenger (000)	Analysis
ResExpCargo	00210 - Directly Assignable To Cargo (000)	Analysis
ResExpOth	00220 - Not Directly Assignable (000)	Analysis
ResExpense	00230 - Total Reservation And Sales Expense (000)	Analysis
//Indirect Operating Expense - Advertising And Publicity Expense
AdExpPax	00250 - Directly Assignable To Passenger (000)	Analysis
AdExpCargo	00260 - Directly Assignable To Cargo (000)	Analysis
AdExpInst	00270 - Institutional Advertising (000)	Analysis
AdExpense	00280 - Total Advertising And Publicity Expense (000)	Analysis
//Indirect Operating Expense - General And Administrative Expense
AdminExpense	00290 - General And Administrative Expense (000)	Analysis
//Indirect Operating Expense - Depreciation Expense - Maintenance Equipment
DeprExpMaint	00340 - Depreciation Expense - Maintenance Equipment (000)	Analysis
//Indirect Operating Expense - Amortization - Other Than Flight Equipment
Amortization	00350 - Amortization (Other Than Flight Equipment) (000)	Analysis
//Transport Related Expense
TransportExp	00370 - Transport Related Expense (000)	Analysis
//Total Operating Expense
TotalOpExpense	00380 - Total Operating Expense (000)	Analysis
//Other
MaintPropEquip	00310 - Maintenance-Ground Property And Equipment (000)	Analysis
DeprPropEquip	00320 - Depreciation-Ground Property And Equipment (000)	Analysis
MaintDepr	00330 - Total Maintenance And Depreciation - Ground (000)	Analysis
SvcSalesOpExp	00360 - Total Servicing, Sales And General Operating Expenses (000)	Analysis
//Carrier Information
AirlineID	An identification number assigned by US DOT to identify a unique airline (carrier). A unique airline (carrier) is defined as one holding and reporting under the same DOT certificate regardless of its Code, Name, or holding company/corporation.	Analysis
UniqueCarrier	Unique Carrier Code. When the same code has been used by multiple carriers, a numeric suffix is used for earlier users, for example, PA, PA(1), PA(2). Use this field for analysis across a range of years.	Analysis
UniqueCarrierName	Unique Carrier Name. When the same name has been used by multiple carriers, a numeric suffix is used for earlier users, for example, Air Caribbean, Air Caribbean (1).	 
Carrier	Code assigned by IATA and commonly used to identify a carrier. As the same code may have been assigned to different carriers over time, the code is not always unique. For analysis, use the Unique Carrier Code.	 
CarrierName	Carrier Name	 
UniqCarrierEntity	Unique Entity for a Carrier's Operation Region.	Analysis
CarrierRegion	Carrier's Operation Region. Carriers Report Data by Operation Region	Analysis
CarrierGroupNew	Carrier Group New	Analysis
CarrierGroup	Carrier Group Code. Used in Legacy Analysis	Analysis
//Time Period Information
Year	Year	 
Quarter	Quarter (1-4)	Analysis"
);

?>