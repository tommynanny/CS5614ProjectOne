<!-- Begin page content -->
<main role="main" class="container">
    <div class="row">
        <div id="timeline_nav"></div>
        <br><br>


        <section id="timeline" class="mt-5">
            <h3 class="display-5"><i class="fas fa-clipboard-list"></i> Timeline</h3>
            <hr>
            <ul class="timeline">
                <li class="timeline">
                    <div class="timeline-badge info"><i class="far fa-handshake"></i></div>
                    <div class="timeline-panel ripple" data-ripple-color="info">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Group Founded</h4>
                            <h5 class="timeline-subtitle">CS5614 Project Group: <?= GROUP_NAME ?></h5>
                            <p><small class="text-muted"><i class="fas fa-calendar-alt"></i> Sep 1st 2020 -
                                    present</small></p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-badge warning"><i class="fas fa-users"></i></div>
                    <div class="timeline-panel ripple" data-ripple-color="warning">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">First Project 1 Meeting With Dr. Soheil Sibdari</h4>
                            <h5 class="timeline-subtitle">Zoom</h5>
                            <p><small class="text-muted"><i class="fas fa-calendar-alt"></i> Sep 18 2020</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p><span>&#8226;</span> Select project topic to be related with "Growth of different
                                Airline industries over the period of time: Visualization</p>
                            <p><span>&#8226;</span> keyword: <b
                                        style="background-color: yellow">Visualization</b>
                            </p>
                            </br>
                            <a type="button" class="btn btn-primary" style="color:white"
                               href="https://docs.google.com/document/d/1h8IVzB5eyWRA-V5RnL0RrFP3QoH9hbsyFhITJhGE6-0/edit?usp=sharing"
                               target="_blank">
                                <i class="fas fa-file-word"></i> Note </a>
                        </div>
                    </div>
                </li>
                <li class="timeline">
                    <div class="timeline-badge success"><i class="fas fa-edit"></i></div>
                    <div class="timeline-panel ripple" data-ripple-color="success">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Project 1 Started</h4>
                            <h5 class="timeline-subtitle">CS5614 Project Group: <?= GROUP_NAME ?></h5>
                            <p><small class="text-muted"><i class="fas fa-calendar-alt"></i> Sep 20,
                                    2020</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p><span>&#8226;</span>Start Project proposal </p>
                            </br>

                            <a type="button" class="btn btn-primary" style="color:white"
                               href="https://docs.google.com/document/d/1eT8A9szCShqrtNvtUu0ystbgHWor7mHa4oPaSP-zcG4/edit?usp=sharing"
                               target="_blank">
                                <i class="fas fa-file-word"></i> Note </a>
                            <a type="button" class="btn btn-primary" style="color:white"
                               href="https://www.overleaf.com/project/5f6a35590af2300001f1ce2c" target="_blank">
                                <i class="fas fa-book"></i> Overleaf </a>
                        </div>
                    </div>
                </li>

                <li class="timeline-inverted">
                    <div class="timeline-badge primary"><i class="fab fa-telegram-plane"></i></div>
                    <div class="timeline-panel ripple" data-ripple-color="primary">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">TBD</h4>
                        </div>
                        <div class="timeline-body">
                            <i class="fas fa-trophy"></i> ... <br>
                        </div>
                    </div>
                </li>

            </ul>
        </section>
    </div>
</main>



