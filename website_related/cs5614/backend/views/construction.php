<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title><?= $pageTitle ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
    <link rel="stylesheet" href="./style.css">
    <!-- Font Awesome https://fontawesome.com/  https://use.fontawesome.com/releases/v5.11.2/js/all.js -->
    <script src="https://use.fontawesome.com/releases/v5.11.2/js/all.js"></script>
    <link rel="icon" href="<?= IMG_URL ?>/fav.png" type="image/icon type">
    <style>
        @import url("https://fonts.googleapis.com/css?family=Poppins");
        /*html, body, div, span, applet, object, iframe,*/
        /*h1, h2, h3, h4, h5, h6, p, blockquote, pre,*/
        /*a, abbr, acronym, address, big, cite, code,*/
        /*del, dfn, em, img, ins, kbd, q, s, samp,*/
        /*small, strike, strong, sub, sup, tt, var,*/
        /*b, u, i, center,*/
        /*dl, dt, dd, ol, ul, li,*/
        /*fieldset, form, label, legend,*/
        /*table, caption, tbody, tfoot, thead, tr, th, td,*/
        /*article, aside, canvas, details, embed,*/
        /*figure, figcaption, footer, header, hgroup,*/
        /*menu, nav, output, ruby, section, summary,*/
        /*time, mark, audio, video {*/
        /*  margin: 0;*/
        /*  padding: 0;*/
        /*  border: 0;*/
        /*  font: inherit;*/
        /*  font-size: 100%;*/
        /*  vertical-align: baseline;*/
        /*}*/

        html {
            line-height: 1;
        }

        ol, ul {
            list-style: none;
        }

        table {
            /*border-collapse: collapse;*/
            border-spacing: 0;
        }

        caption, th, td {
            text-align: left;
            font-weight: normal;
            vertical-align: middle;
        }

        q, blockquote {
            quotes: none;
        }

        q:before, q:after, blockquote:before, blockquote:after {
            content: "";
            content: none;
        }

        a img {
            border: none;
        }

        article, aside, details, figcaption, figure, footer, hgroup, main, menu, nav, section, summary {
            display: block;
        }

        @keyframes shining {
            0% {
                opacity: 1;
            }
            100% {
                opacity: 0.2;
            }
        }

        body {
            background-color: #482472;
            font-family: Poppins;
        }

        .scenery {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 100%;
            /*height: 618px;*/
            height: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            transform: translate(-50%, -50%);
            overflow: hidden;
            border-radius: 5px;
            color: #fff;
            background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuNSIgeTE9IjAuMCIgeDI9IjAuNSIgeTI9IjEuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzJhMWY2ZiIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2FlMzA4MiIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
            background-size: 100%;
            background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #2a1f6f), color-stop(100%, #ae3082));
            background-image: -moz-linear-gradient(#2a1f6f, #ae3082);
            background-image: -webkit-linear-gradient(#2a1f6f, #ae3082);
            background-image: linear-gradient(#2a1f6f, #ae3082);
            -moz-box-shadow: rgba(0, 0, 0, 0.8) 0px 0px 100px;
            -webkit-box-shadow: rgba(0, 0, 0, 0.8) 0px 0px 100px;
            box-shadow: rgba(0, 0, 0, 0.8) 0px 0px 100px;

        }

        .star {
            position: absolute;
            z-index: 1;
            background-color: #fff;
            animation-name: shining;
            animation-timing-function: ease;
            animation-direction: alternate;
            animation-iteration-count: infinite;
            -moz-box-shadow: #fff 0px 0px 5px;
            -webkit-box-shadow: #fff 0px 0px 5px;
            box-shadow: #fff 0px 0px 5px;
            -moz-border-radius: 500px;
            -webkit-border-radius: 500px;
            border-radius: 500px;
        }

        .star:nth-child(1) {
            width: 3px;
            height: 3px;
            bottom: 173px;
            left: 960px;
            animation-duration: 1s;
        }

        .star:nth-child(2) {
            width: 2px;
            height: 2px;
            bottom: 388px;
            left: 985px;
            animation-duration: 4s;
        }

        .star:nth-child(3) {
            width: 1px;
            height: 1px;
            bottom: 262px;
            left: 708px;
            animation-duration: 0.5s;
        }

        .star:nth-child(4) {
            width: 1px;
            height: 1px;
            bottom: 576px;
            left: 202px;
            animation-duration: 2s;
        }

        .star:nth-child(5) {
            width: 2px;
            height: 2px;
            bottom: 285px;
            left: 885px;
            animation-duration: 2s;
        }

        .star:nth-child(6) {
            width: 3px;
            height: 3px;
            bottom: 200px;
            left: 426px;
            animation-duration: 1.33333s;
        }

        .star:nth-child(7) {
            width: 2px;
            height: 2px;
            bottom: 282px;
            left: 37px;
            animation-duration: 0.66667s;
        }

        .star:nth-child(8) {
            width: 3px;
            height: 3px;
            bottom: 55px;
            left: 301px;
            animation-duration: 1s;
        }

        .star:nth-child(9) {
            width: 2px;
            height: 2px;
            bottom: 38px;
            left: 785px;
            animation-duration: 1s;
        }

        .star:nth-child(10) {
            width: 1px;
            height: 1px;
            bottom: 120px;
            left: 23px;
            animation-duration: 3s;
        }

        .star:nth-child(11) {
            width: 1px;
            height: 1px;
            bottom: 429px;
            left: 329px;
            animation-duration: 0.66667s;
        }

        .star:nth-child(12) {
            width: 1px;
            height: 1px;
            bottom: 601px;
            left: 284px;
            animation-duration: 0.33333s;
        }

        .star:nth-child(13) {
            width: 3px;
            height: 3px;
            bottom: 596px;
            left: 767px;
            animation-duration: 2s;
        }

        .star:nth-child(14) {
            width: 1px;
            height: 1px;
            bottom: 524px;
            left: 260px;
            animation-duration: 4s;
        }

        .star:nth-child(15) {
            width: 1px;
            height: 1px;
            bottom: 67px;
            left: 790px;
            animation-duration: 0.66667s;
        }

        .star:nth-child(16) {
            width: 1px;
            height: 1px;
            bottom: 159px;
            left: 939px;
            animation-duration: 2s;
        }

        .star:nth-child(17) {
            width: 1px;
            height: 1px;
            bottom: 405px;
            left: 978px;
            animation-duration: 1.5s;
        }

        .star:nth-child(18) {
            width: 2px;
            height: 2px;
            bottom: 507px;
            left: 390px;
            animation-duration: 1s;
        }

        .star:nth-child(19) {
            width: 3px;
            height: 3px;
            bottom: 479px;
            left: 268px;
            animation-duration: 2s;
        }

        .star:nth-child(20) {
            width: 2px;
            height: 2px;
            bottom: 304px;
            left: 712px;
            animation-duration: 1s;
        }

        .star:nth-child(21) {
            width: 1px;
            height: 1px;
            bottom: 367px;
            left: 469px;
            animation-duration: 0.66667s;
        }

        .star:nth-child(22) {
            width: 2px;
            height: 2px;
            bottom: 443px;
            left: 72px;
            animation-duration: 0.66667s;
        }

        .star:nth-child(23) {
            width: 2px;
            height: 2px;
            bottom: 190px;
            left: 764px;
            animation-duration: 4s;
        }

        .star:nth-child(24) {
            width: 2px;
            height: 2px;
            bottom: 610px;
            left: 322px;
            animation-duration: 1.5s;
        }

        .star:nth-child(25) {
            width: 2px;
            height: 2px;
            bottom: 62px;
            left: 190px;
            animation-duration: 1s;
        }

        .star:nth-child(26) {
            width: 1px;
            height: 1px;
            bottom: 363px;
            left: 169px;
            animation-duration: 2s;
        }

        .star:nth-child(27) {
            width: 2px;
            height: 2px;
            bottom: 66px;
            left: 579px;
            animation-duration: 2s;
        }

        .star:nth-child(28) {
            width: 1px;
            height: 1px;
            bottom: 618px;
            left: 878px;
            animation-duration: 1.5s;
        }

        .star:nth-child(29) {
            width: 1px;
            height: 1px;
            bottom: 186px;
            left: 971px;
            animation-duration: 1s;
        }

        .star:nth-child(30) {
            width: 2px;
            height: 2px;
            bottom: 311px;
            left: 885px;
            animation-duration: 0.5s;
        }

        .star:nth-child(31) {
            width: 3px;
            height: 3px;
            bottom: 86px;
            left: 269px;
            animation-duration: 1.33333s;
        }

        .star:nth-child(32) {
            width: 2px;
            height: 2px;
            bottom: 108px;
            left: 394px;
            animation-duration: 1.33333s;
        }

        .star:nth-child(33) {
            width: 2px;
            height: 2px;
            bottom: 324px;
            left: 712px;
            animation-duration: 0.66667s;
        }

        .star:nth-child(34) {
            width: 1px;
            height: 1px;
            bottom: 579px;
            left: 366px;
            animation-duration: 1.5s;
        }

        .star:nth-child(35) {
            width: 1px;
            height: 1px;
            bottom: 387px;
            left: 160px;
            animation-duration: 2s;
        }

        .star:nth-child(36) {
            width: 1px;
            height: 1px;
            bottom: 507px;
            left: 402px;
            animation-duration: 1s;
        }

        .star:nth-child(37) {
            width: 2px;
            height: 2px;
            bottom: 520px;
            left: 338px;
            animation-duration: 1s;
        }

        .star:nth-child(38) {
            width: 1px;
            height: 1px;
            bottom: 474px;
            left: 870px;
            animation-duration: 1s;
        }

        .star:nth-child(39) {
            width: 2px;
            height: 2px;
            bottom: 329px;
            left: 317px;
            animation-duration: 2s;
        }

        .star:nth-child(40) {
            width: 1px;
            height: 1px;
            bottom: 121px;
            left: 934px;
            animation-duration: 0.66667s;
        }

        .star:nth-child(41) {
            width: 2px;
            height: 2px;
            bottom: 497px;
            left: 281px;
            animation-duration: 0.33333s;
        }

        .star:nth-child(42) {
            width: 3px;
            height: 3px;
            bottom: 530px;
            left: 345px;
            animation-duration: 1s;
        }

        .star:nth-child(43) {
            width: 2px;
            height: 2px;
            bottom: 536px;
            left: 357px;
            animation-duration: 0.5s;
        }

        .star:nth-child(44) {
            width: 1px;
            height: 1px;
            bottom: 208px;
            left: 324px;
            animation-duration: 0.33333s;
        }

        .star:nth-child(45) {
            width: 3px;
            height: 3px;
            bottom: 18px;
            left: 124px;
            animation-duration: 1s;
        }

        .star:nth-child(46) {
            width: 3px;
            height: 3px;
            bottom: 326px;
            left: 714px;
            animation-duration: 3s;
        }

        .star:nth-child(47) {
            width: 1px;
            height: 1px;
            bottom: 397px;
            left: 491px;
            animation-duration: 1.33333s;
        }

        .star:nth-child(48) {
            width: 1px;
            height: 1px;
            bottom: 175px;
            left: 709px;
            animation-duration: 4s;
        }

        .star:nth-child(49) {
            width: 2px;
            height: 2px;
            bottom: 38px;
            left: 121px;
            animation-duration: 1.33333s;
        }

        .star:nth-child(50) {
            width: 1px;
            height: 1px;
            bottom: 608px;
            left: 432px;
            animation-duration: 1s;
        }

        .star:nth-child(51) {
            width: 1px;
            height: 1px;
            bottom: 595px;
            left: 236px;
            animation-duration: 0.33333s;
        }

        .star:nth-child(52) {
            width: 1px;
            height: 1px;
            bottom: 74px;
            left: 169px;
            animation-duration: 1s;
        }

        .star:nth-child(53) {
            width: 3px;
            height: 3px;
            bottom: 316px;
            left: 907px;
            animation-duration: 2s;
        }

        .star:nth-child(54) {
            width: 3px;
            height: 3px;
            bottom: 317px;
            left: 846px;
            animation-duration: 4s;
        }

        .star:nth-child(55) {
            width: 2px;
            height: 2px;
            bottom: 51px;
            left: 300px;
            animation-duration: 1s;
        }

        .star:nth-child(56) {
            width: 1px;
            height: 1px;
            bottom: 291px;
            left: 525px;
            animation-duration: 1.33333s;
        }

        .star:nth-child(57) {
            width: 3px;
            height: 3px;
            bottom: 576px;
            left: 978px;
            animation-duration: 4s;
        }

        .star:nth-child(58) {
            width: 3px;
            height: 3px;
            bottom: 302px;
            left: 478px;
            animation-duration: 0.66667s;
        }

        .star:nth-child(59) {
            width: 3px;
            height: 3px;
            bottom: 575px;
            left: 589px;
            animation-duration: 1s;
        }

        .star:nth-child(60) {
            width: 1px;
            height: 1px;
            bottom: 349px;
            left: 526px;
            animation-duration: 0.33333s;
        }

        .star:nth-child(61) {
            width: 3px;
            height: 3px;
            bottom: 62px;
            left: 480px;
            animation-duration: 1s;
        }

        .star:nth-child(62) {
            width: 3px;
            height: 3px;
            bottom: 495px;
            left: 688px;
            animation-duration: 2s;
        }

        .star:nth-child(63) {
            width: 2px;
            height: 2px;
            bottom: 162px;
            left: 412px;
            animation-duration: 1.33333s;
        }

        .star:nth-child(64) {
            width: 1px;
            height: 1px;
            bottom: 492px;
            left: 266px;
            animation-duration: 1s;
        }

        .star:nth-child(65) {
            width: 2px;
            height: 2px;
            bottom: 275px;
            left: 835px;
            animation-duration: 3s;
        }

        .star:nth-child(66) {
            width: 2px;
            height: 2px;
            bottom: 547px;
            left: 909px;
            animation-duration: 0.33333s;
        }

        .star:nth-child(67) {
            width: 1px;
            height: 1px;
            bottom: 391px;
            left: 286px;
            animation-duration: 1.33333s;
        }

        .star:nth-child(68) {
            width: 1px;
            height: 1px;
            bottom: 563px;
            left: 818px;
            animation-duration: 2s;
        }

        .star:nth-child(69) {
            width: 2px;
            height: 2px;
            bottom: 601px;
            left: 741px;
            animation-duration: 1s;
        }

        .star:nth-child(70) {
            width: 2px;
            height: 2px;
            bottom: 162px;
            left: 768px;
            animation-duration: 4s;
        }

        .star:nth-child(71) {
            width: 2px;
            height: 2px;
            bottom: 608px;
            left: 1px;
            animation-duration: 1s;
        }

        .star:nth-child(72) {
            width: 3px;
            height: 3px;
            bottom: 297px;
            left: 152px;
            animation-duration: 1.5s;
        }

        .star:nth-child(73) {
            width: 3px;
            height: 3px;
            bottom: 103px;
            left: 545px;
            animation-duration: 2s;
        }

        .star:nth-child(74) {
            width: 1px;
            height: 1px;
            bottom: 177px;
            left: 772px;
            animation-duration: 1s;
        }

        .star:nth-child(75) {
            width: 3px;
            height: 3px;
            bottom: 460px;
            left: 109px;
            animation-duration: 2s;
        }

        .star:nth-child(76) {
            width: 2px;
            height: 2px;
            bottom: 372px;
            left: 707px;
            animation-duration: 0.33333s;
        }

        .star:nth-child(77) {
            width: 2px;
            height: 2px;
            bottom: 327px;
            left: 703px;
            animation-duration: 0.66667s;
        }

        .star:nth-child(78) {
            width: 2px;
            height: 2px;
            bottom: 117px;
            left: 74px;
            animation-duration: 1s;
        }

        .star:nth-child(79) {
            width: 3px;
            height: 3px;
            bottom: 355px;
            left: 153px;
            animation-duration: 1s;
        }

        .star:nth-child(80) {
            width: 3px;
            height: 3px;
            bottom: 258px;
            left: 623px;
            animation-duration: 4s;
        }

        .star:nth-child(81) {
            width: 2px;
            height: 2px;
            bottom: 347px;
            left: 584px;
            animation-duration: 1s;
        }

        .star:nth-child(82) {
            width: 3px;
            height: 3px;
            bottom: 121px;
            left: 551px;
            animation-duration: 4s;
        }

        .star:nth-child(83) {
            width: 1px;
            height: 1px;
            bottom: 41px;
            left: 696px;
            animation-duration: 1s;
        }

        .star:nth-child(84) {
            width: 2px;
            height: 2px;
            bottom: 124px;
            left: 304px;
            animation-duration: 1s;
        }

        .star:nth-child(85) {
            width: 1px;
            height: 1px;
            bottom: 502px;
            left: 860px;
            animation-duration: 1.5s;
        }

        .star:nth-child(86) {
            width: 2px;
            height: 2px;
            bottom: 384px;
            left: 406px;
            animation-duration: 4s;
        }

        .star:nth-child(87) {
            width: 2px;
            height: 2px;
            bottom: 410px;
            left: 504px;
            animation-duration: 3s;
        }

        .star:nth-child(88) {
            width: 2px;
            height: 2px;
            bottom: 155px;
            left: 799px;
            animation-duration: 2s;
        }

        .star:nth-child(89) {
            width: 2px;
            height: 2px;
            bottom: 438px;
            left: 818px;
            animation-duration: 4s;
        }

        .star:nth-child(90) {
            width: 2px;
            height: 2px;
            bottom: 586px;
            left: 804px;
            animation-duration: 1s;
        }

        .star:nth-child(91) {
            width: 3px;
            height: 3px;
            bottom: 360px;
            left: 290px;
            animation-duration: 2s;
        }

        .star:nth-child(92) {
            width: 3px;
            height: 3px;
            bottom: 602px;
            left: 454px;
            animation-duration: 0.66667s;
        }

        .star:nth-child(93) {
            width: 2px;
            height: 2px;
            bottom: 274px;
            left: 187px;
            animation-duration: 1.5s;
        }

        .star:nth-child(94) {
            width: 2px;
            height: 2px;
            bottom: 92px;
            left: 518px;
            animation-duration: 2s;
        }

        .star:nth-child(95) {
            width: 3px;
            height: 3px;
            bottom: 26px;
            left: 914px;
            animation-duration: 1s;
        }

        .star:nth-child(96) {
            width: 2px;
            height: 2px;
            bottom: 432px;
            left: 923px;
            animation-duration: 1.33333s;
        }

        .star:nth-child(97) {
            width: 3px;
            height: 3px;
            bottom: 163px;
            left: 256px;
            animation-duration: 1s;
        }

        .star:nth-child(98) {
            width: 2px;
            height: 2px;
            bottom: 448px;
            left: 316px;
            animation-duration: 2s;
        }

        .star:nth-child(99) {
            width: 2px;
            height: 2px;
            bottom: 306px;
            left: 495px;
            animation-duration: 0.33333s;
        }


        .stars2 {
            position: fixed;
            left: 50%;
            top: 50%;
        }

        .star2 {
            position: relative;
            z-index: 1;
            background-color: #fff;
            animation-name: shining;
            animation-timing-function: ease;
            animation-direction: alternate;
            animation-iteration-count: infinite;
            -moz-box-shadow: #fff 0px 0px 5px;
            -webkit-box-shadow: #fff 0px 0px 5px;
            box-shadow: #fff 0px 0px 5px;
            -moz-border-radius: 500px;
            -webkit-border-radius: 500px;
            border-radius: 500px;
        }

        .star2:nth-child(1) {
            width: 3px;
            height: 3px;
            bottom: 173px;
            left: 960px;
            animation-duration: 1s;
        }

        .star2:nth-child(2) {
            width: 2px;
            height: 2px;
            bottom: 388px;
            left: 985px;
            animation-duration: 4s;
        }

        .star2:nth-child(3) {
            width: 1px;
            height: 1px;
            bottom: 262px;
            left: 708px;
            animation-duration: 0.5s;
        }

        .star2:nth-child(4) {
            width: 1px;
            height: 1px;
            bottom: 576px;
            left: 202px;
            animation-duration: 2s;
        }

        .star2:nth-child(5) {
            width: 2px;
            height: 2px;
            bottom: 285px;
            left: 885px;
            animation-duration: 2s;
        }

        .star2:nth-child(6) {
            width: 3px;
            height: 3px;
            bottom: 200px;
            left: 426px;
            animation-duration: 1.33333s;
        }

        .star2:nth-child(7) {
            width: 2px;
            height: 2px;
            bottom: 282px;
            left: 37px;
            animation-duration: 0.66667s;
        }

        .star2:nth-child(8) {
            width: 3px;
            height: 3px;
            bottom: 55px;
            left: 301px;
            animation-duration: 1s;
        }

        .star2:nth-child(9) {
            width: 2px;
            height: 2px;
            bottom: 38px;
            left: 785px;
            animation-duration: 1s;
        }

        .star2:nth-child(10) {
            width: 1px;
            height: 1px;
            bottom: 120px;
            left: 23px;
            animation-duration: 3s;
        }

        .star2:nth-child(11) {
            width: 1px;
            height: 1px;
            bottom: 429px;
            left: 329px;
            animation-duration: 0.66667s;
        }

        .star2:nth-child(12) {
            width: 1px;
            height: 1px;
            bottom: 601px;
            left: 284px;
            animation-duration: 0.33333s;
        }

        .star2:nth-child(13) {
            width: 3px;
            height: 3px;
            bottom: 596px;
            left: 767px;
            animation-duration: 2s;
        }

        .star2:nth-child(14) {
            width: 1px;
            height: 1px;
            bottom: 524px;
            left: 260px;
            animation-duration: 4s;
        }

        .star2:nth-child(15) {
            width: 1px;
            height: 1px;
            bottom: 67px;
            left: 790px;
            animation-duration: 0.66667s;
        }

        .star2:nth-child(16) {
            width: 1px;
            height: 1px;
            bottom: 159px;
            left: 939px;
            animation-duration: 2s;
        }

        .star2:nth-child(17) {
            width: 1px;
            height: 1px;
            bottom: 405px;
            left: 978px;
            animation-duration: 1.5s;
        }

        .star2:nth-child(18) {
            width: 2px;
            height: 2px;
            bottom: 507px;
            left: 390px;
            animation-duration: 1s;
        }

        .star2:nth-child(19) {
            width: 3px;
            height: 3px;
            bottom: 479px;
            left: 268px;
            animation-duration: 2s;
        }

        .star2:nth-child(20) {
            width: 2px;
            height: 2px;
            bottom: 304px;
            left: 712px;
            animation-duration: 1s;
        }

        .star2:nth-child(21) {
            width: 1px;
            height: 1px;
            bottom: 367px;
            left: 469px;
            animation-duration: 0.66667s;
        }

        .star2:nth-child(22) {
            width: 2px;
            height: 2px;
            bottom: 443px;
            left: 72px;
            animation-duration: 0.66667s;
        }

        .star2:nth-child(23) {
            width: 2px;
            height: 2px;
            bottom: 190px;
            left: 764px;
            animation-duration: 4s;
        }

        .star2:nth-child(24) {
            width: 2px;
            height: 2px;
            bottom: 610px;
            left: 322px;
            animation-duration: 1.5s;
        }

        .star2:nth-child(25) {
            width: 2px;
            height: 2px;
            bottom: 62px;
            left: 190px;
            animation-duration: 1s;
        }

        .star2:nth-child(26) {
            width: 1px;
            height: 1px;
            bottom: 363px;
            left: 169px;
            animation-duration: 2s;
        }

        .star2:nth-child(27) {
            width: 2px;
            height: 2px;
            bottom: 66px;
            left: 579px;
            animation-duration: 2s;
        }

        .star2:nth-child(28) {
            width: 1px;
            height: 1px;
            bottom: 618px;
            left: 878px;
            animation-duration: 1.5s;
        }

        .star2:nth-child(29) {
            width: 1px;
            height: 1px;
            bottom: 186px;
            left: 971px;
            animation-duration: 1s;
        }

        .star2:nth-child(30) {
            width: 2px;
            height: 2px;
            bottom: 311px;
            left: 885px;
            animation-duration: 0.5s;
        }

        .star2:nth-child(31) {
            width: 3px;
            height: 3px;
            bottom: 86px;
            left: 269px;
            animation-duration: 1.33333s;
        }

        .star2:nth-child(32) {
            width: 2px;
            height: 2px;
            bottom: 108px;
            left: 394px;
            animation-duration: 1.33333s;
        }

        .star2:nth-child(33) {
            width: 2px;
            height: 2px;
            bottom: 324px;
            left: 712px;
            animation-duration: 0.66667s;
        }

        .star2:nth-child(34) {
            width: 1px;
            height: 1px;
            bottom: 579px;
            left: 366px;
            animation-duration: 1.5s;
        }

        .star2:nth-child(35) {
            width: 1px;
            height: 1px;
            bottom: 387px;
            left: 160px;
            animation-duration: 2s;
        }

        .star2:nth-child(36) {
            width: 1px;
            height: 1px;
            bottom: 507px;
            left: 402px;
            animation-duration: 1s;
        }

        .star2:nth-child(37) {
            width: 2px;
            height: 2px;
            bottom: 520px;
            left: 338px;
            animation-duration: 1s;
        }

        .star2:nth-child(38) {
            width: 1px;
            height: 1px;
            bottom: 474px;
            left: 870px;
            animation-duration: 1s;
        }

        .star2:nth-child(39) {
            width: 2px;
            height: 2px;
            bottom: 329px;
            left: 317px;
            animation-duration: 2s;
        }

        .star2:nth-child(40) {
            width: 1px;
            height: 1px;
            bottom: 121px;
            left: 934px;
            animation-duration: 0.66667s;
        }

        .star2:nth-child(41) {
            width: 2px;
            height: 2px;
            bottom: 497px;
            left: 281px;
            animation-duration: 0.33333s;
        }

        .star2:nth-child(42) {
            width: 3px;
            height: 3px;
            bottom: 530px;
            left: 345px;
            animation-duration: 1s;
        }

        .star2:nth-child(43) {
            width: 2px;
            height: 2px;
            bottom: 536px;
            left: 357px;
            animation-duration: 0.5s;
        }

        .star2:nth-child(44) {
            width: 1px;
            height: 1px;
            bottom: 208px;
            left: 324px;
            animation-duration: 0.33333s;
        }

        .star2:nth-child(45) {
            width: 3px;
            height: 3px;
            bottom: 18px;
            left: 124px;
            animation-duration: 1s;
        }

        .star2:nth-child(46) {
            width: 3px;
            height: 3px;
            bottom: 326px;
            left: 714px;
            animation-duration: 3s;
        }

        .star2:nth-child(47) {
            width: 1px;
            height: 1px;
            bottom: 397px;
            left: 491px;
            animation-duration: 1.33333s;
        }

        .star2:nth-child(48) {
            width: 1px;
            height: 1px;
            bottom: 175px;
            left: 709px;
            animation-duration: 4s;
        }

        .star2:nth-child(49) {
            width: 2px;
            height: 2px;
            bottom: 38px;
            left: 121px;
            animation-duration: 1.33333s;
        }

        .star2:nth-child(50) {
            width: 1px;
            height: 1px;
            bottom: 608px;
            left: 432px;
            animation-duration: 1s;
        }

        .star2:nth-child(51) {
            width: 1px;
            height: 1px;
            bottom: 595px;
            left: 236px;
            animation-duration: 0.33333s;
        }

        .star2:nth-child(52) {
            width: 1px;
            height: 1px;
            bottom: 74px;
            left: 169px;
            animation-duration: 1s;
        }

        .star2:nth-child(53) {
            width: 3px;
            height: 3px;
            bottom: 316px;
            left: 907px;
            animation-duration: 2s;
        }

        .star2:nth-child(54) {
            width: 3px;
            height: 3px;
            bottom: 317px;
            left: 846px;
            animation-duration: 4s;
        }

        .star2:nth-child(55) {
            width: 2px;
            height: 2px;
            bottom: 51px;
            left: 300px;
            animation-duration: 1s;
        }

        .star2:nth-child(56) {
            width: 1px;
            height: 1px;
            bottom: 291px;
            left: 525px;
            animation-duration: 1.33333s;
        }

        .star2:nth-child(57) {
            width: 3px;
            height: 3px;
            bottom: 576px;
            left: 978px;
            animation-duration: 4s;
        }

        .star2:nth-child(58) {
            width: 3px;
            height: 3px;
            bottom: 302px;
            left: 478px;
            animation-duration: 0.66667s;
        }

        .star2:nth-child(59) {
            width: 3px;
            height: 3px;
            bottom: 575px;
            left: 589px;
            animation-duration: 1s;
        }

        .star2:nth-child(60) {
            width: 1px;
            height: 1px;
            bottom: 349px;
            left: 526px;
            animation-duration: 0.33333s;
        }

        .star2:nth-child(61) {
            width: 3px;
            height: 3px;
            bottom: 62px;
            left: 480px;
            animation-duration: 1s;
        }

        .star2:nth-child(62) {
            width: 3px;
            height: 3px;
            bottom: 495px;
            left: 688px;
            animation-duration: 2s;
        }

        .star2:nth-child(63) {
            width: 2px;
            height: 2px;
            bottom: 162px;
            left: 412px;
            animation-duration: 1.33333s;
        }

        .star2:nth-child(64) {
            width: 1px;
            height: 1px;
            bottom: 492px;
            left: 266px;
            animation-duration: 1s;
        }

        .star2:nth-child(65) {
            width: 2px;
            height: 2px;
            bottom: 275px;
            left: 835px;
            animation-duration: 3s;
        }

        .star2:nth-child(66) {
            width: 2px;
            height: 2px;
            bottom: 547px;
            left: 909px;
            animation-duration: 0.33333s;
        }

        .star2:nth-child(67) {
            width: 1px;
            height: 1px;
            bottom: 391px;
            left: 286px;
            animation-duration: 1.33333s;
        }

        .star2:nth-child(68) {
            width: 1px;
            height: 1px;
            bottom: 563px;
            left: 818px;
            animation-duration: 2s;
        }

        .star2:nth-child(69) {
            width: 2px;
            height: 2px;
            bottom: 601px;
            left: 741px;
            animation-duration: 1s;
        }

        .star2:nth-child(70) {
            width: 2px;
            height: 2px;
            bottom: 162px;
            left: 768px;
            animation-duration: 4s;
        }

        .star2:nth-child(71) {
            width: 2px;
            height: 2px;
            bottom: 608px;
            left: 1px;
            animation-duration: 1s;
        }

        .star2:nth-child(72) {
            width: 3px;
            height: 3px;
            bottom: 297px;
            left: 152px;
            animation-duration: 1.5s;
        }

        .star2:nth-child(73) {
            width: 3px;
            height: 3px;
            bottom: 103px;
            left: 545px;
            animation-duration: 2s;
        }

        .star2:nth-child(74) {
            width: 1px;
            height: 1px;
            bottom: 177px;
            left: 772px;
            animation-duration: 1s;
        }

        .star2:nth-child(75) {
            width: 3px;
            height: 3px;
            bottom: 460px;
            left: 109px;
            animation-duration: 2s;
        }

        .star2:nth-child(76) {
            width: 2px;
            height: 2px;
            bottom: 372px;
            left: 707px;
            animation-duration: 0.33333s;
        }

        .star2:nth-child(77) {
            width: 2px;
            height: 2px;
            bottom: 327px;
            left: 703px;
            animation-duration: 0.66667s;
        }

        .star2:nth-child(78) {
            width: 2px;
            height: 2px;
            bottom: 117px;
            left: 74px;
            animation-duration: 1s;
        }

        .star2:nth-child(79) {
            width: 3px;
            height: 3px;
            bottom: 355px;
            left: 153px;
            animation-duration: 1s;
        }

        .star2:nth-child(80) {
            width: 3px;
            height: 3px;
            bottom: 258px;
            left: 623px;
            animation-duration: 4s;
        }

        .star2:nth-child(81) {
            width: 2px;
            height: 2px;
            bottom: 347px;
            left: 584px;
            animation-duration: 1s;
        }

        .star2:nth-child(82) {
            width: 3px;
            height: 3px;
            bottom: 121px;
            left: 551px;
            animation-duration: 4s;
        }

        .star2:nth-child(83) {
            width: 1px;
            height: 1px;
            bottom: 41px;
            left: 696px;
            animation-duration: 1s;
        }

        .star2:nth-child(84) {
            width: 2px;
            height: 2px;
            bottom: 124px;
            left: 304px;
            animation-duration: 1s;
        }

        .star2:nth-child(85) {
            width: 1px;
            height: 1px;
            bottom: 502px;
            left: 860px;
            animation-duration: 1.5s;
        }

        .star2:nth-child(86) {
            width: 2px;
            height: 2px;
            bottom: 384px;
            left: 406px;
            animation-duration: 4s;
        }

        .star2:nth-child(87) {
            width: 2px;
            height: 2px;
            bottom: 410px;
            left: 504px;
            animation-duration: 3s;
        }

        .star2:nth-child(88) {
            width: 2px;
            height: 2px;
            bottom: 155px;
            left: 799px;
            animation-duration: 2s;
        }

        .star2:nth-child(89) {
            width: 2px;
            height: 2px;
            bottom: 438px;
            left: 818px;
            animation-duration: 4s;
        }

        .star2:nth-child(90) {
            width: 2px;
            height: 2px;
            bottom: 586px;
            left: 804px;
            animation-duration: 1s;
        }

        .star2:nth-child(91) {
            width: 3px;
            height: 3px;
            bottom: 360px;
            left: 290px;
            animation-duration: 2s;
        }

        .star2:nth-child(92) {
            width: 3px;
            height: 3px;
            bottom: 602px;
            left: 454px;
            animation-duration: 0.66667s;
        }

        .star2:nth-child(93) {
            width: 2px;
            height: 2px;
            bottom: 274px;
            left: 187px;
            animation-duration: 1.5s;
        }

        .star2:nth-child(94) {
            width: 2px;
            height: 2px;
            bottom: 92px;
            left: 518px;
            animation-duration: 2s;
        }

        .star2:nth-child(95) {
            width: 3px;
            height: 3px;
            bottom: 26px;
            left: 914px;
            animation-duration: 1s;
        }

        .star2:nth-child(96) {
            width: 2px;
            height: 2px;
            bottom: 432px;
            left: 923px;
            animation-duration: 1.33333s;
        }

        .star2:nth-child(97) {
            width: 3px;
            height: 3px;
            bottom: 163px;
            left: 256px;
            animation-duration: 1s;
        }

        .star2:nth-child(98) {
            width: 2px;
            height: 2px;
            bottom: 448px;
            left: 316px;
            animation-duration: 2s;
        }

        .star2:nth-child(99) {
            width: 2px;
            height: 2px;
            bottom: 306px;
            left: 495px;
            animation-duration: 0.33333s;
        }

        .title {
            position: relative;
            z-index: 50;
        }

        .title h1 {
            font-size: 15rem;
            font-weight: bold;
        }

        .title h1 span {
            vertical-align: middle;
            margin: 0 30px;
        }

        .message {
            margin-top: 50px;
        }

        .message h2 {
            font-size: 1.8rem;
        }

        .message h3 {
            color: #22E5F1;
            font-size: 1.4rem;
        }

        .message h4 {
            font-size: 1.2rem;
        }

        .yellow {
            color: yellow;
        }

        .action {
            margin-top: 30px;
        }

        .action button.refresh {
            padding: 10px 20px;
            font-family: Poppins;
            font-size: 1.2rem;
            color: #fff;
            border: 2px solid #ff9c61;
            background-color: #ff9c61;
            border-radius: 50px;
            -moz-transition: all 0.2s ease;
            -o-transition: all 0.2s ease;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease;
            box-shadow: 0px 0px 15px #ff9c61;
        }

        .action button.continue {
            margin-left: 20px;
            padding: 10px 20px;
            font-family: Poppins;
            font-size: 1.2rem;
            color: #fff;
            border: 2px solid #57595D;
            background-color: #57595D;
            border-radius: 50px;
            -moz-transition: all 0.2s ease;
            -o-transition: all 0.2s ease;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease;
            box-shadow: 0px 0px 15px #57595D;
        }

        .action button:hover {
            cursor: pointer;
            background-color: transparent;
        }

        .square {
            width: 150px;
            height: 150px;
            display: inline-block;
            position: relative;
            vertical-align: middle;
            left: -30px;
            -moz-transform: scale(1.1) rotate(45deg);
            -ms-transform: scale(1.1) rotate(45deg);
            -webkit-transform: scale(1.1) rotate(45deg);
            transform: scale(1.1) rotate(45deg);
        }

        .square > * {
            position: absolute;
        }

        .square .light {
            box-shadow: 0px 0px 10px #fedbae;
            background-color: #fedbae;
        }

        .square .light1, .square .light3 {
            width: 15px;
            height: 135px;
            -moz-transform: skew(0deg, 45deg);
            -ms-transform: skew(0deg, 45deg);
            -webkit-transform: skew(0deg, 45deg);
            transform: skew(0deg, 45deg);
        }

        .square .light2, .square .light4 {
            width: 120px;
            height: 15px;
            -moz-transform: skew(45deg);
            -ms-transform: skew(45deg);
            -webkit-transform: skew(45deg);
            transform: skew(45deg);
        }

        .square .light1 {
            z-index: 10;
            top: 5.5px;
            left: 15px;
        }

        .square .light3 {
            z-index: 2;
            top: 7.5px;
            right: 15px;
        }

        .square .light2 {
            top: 0;
            left: 22.5px;
        }

        .square .light4 {
            bottom: 0;
            right: 22.5px;
        }

        .square .shadow {
            background-color: #ff9c61;
            box-shadow: 0px 0px 10px #ff9c61;
        }

        .square .shadow1 {
            bottom: 7.5px;
            right: 0;
            width: 15px;
            height: 120px;
            -moz-transform: skew(0deg, -45deg);
            -ms-transform: skew(0deg, -45deg);
            -webkit-transform: skew(0deg, -45deg);
            transform: skew(0deg, -45deg);
        }

        .square .shadow2 {
            z-index: 3;
            top: 15px;
            left: 22.5px;
            width: 120px;
            height: 15px;
            -moz-transform: skew(-45deg);
            -ms-transform: skew(-45deg);
            -webkit-transform: skew(-45deg);
            transform: skew(-45deg);
        }

        .square .stairs1 li, .square .stairs2 li, .square .stairs3 li, .square .stairs4 li {
            position: relative;
            width: 7.5px;
            height: 7.5px;
            background-color: #fedbae;
            box-shadow: 0px 0px 10px #fedbae;
            -moz-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        .square .stairs1 li::before, .square .stairs2 li::before {
            content: "";
            position: absolute;
            left: 0;
            width: 7.5px;
            height: 13px;
            background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuMCIgeTE9IjAuNSIgeDI9IjEuMCIgeTI9IjAuNSI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZlZGJhZSIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2ZmOWM2MSIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
            background-size: 100%;
            background-image: -webkit-gradient(linear, 0% 50%, 100% 50%, color-stop(0%, #fedbae), color-stop(100%, #ff9c61));
            background-image: -moz-linear-gradient(left, #fedbae, #ff9c61);
            background-image: -webkit-linear-gradient(left, #fedbae, #ff9c61);
            background-image: linear-gradient(to right, #fedbae, #ff9c61);
            box-shadow: 0px 0px 10px #fedbae;
        }

        .square .stairs1 {
            top: 1px;
            left: 11px;
            width: 0px;
            height: 150px;
        }

        .square .stairs1 li {
            margin-bottom: 3px;
        }

        .square .stairs1 li::before {
            top: 100%;
        }

        .square .stairs2 {
            z-index: 10;
            bottom: 19px;
            left: 3px;
            width: 150px;
            height: 0px;
            display: flex;
        }

        .square .stairs2 li {
            margin-right: 3.5px;
        }

        .square .stairs2 li:last-child::before {
            background: #fedbae;
        }

        .square .stairs2 li::before {
            bottom: 100%;
        }

        .square .stairs3 {
            bottom: -32px;
            right: 34px;
            width: 0px;
            height: 150px;
            z-index: 10;
        }

        .square .stairs3 li {
            margin-bottom: 3.5px;
        }

        .square .stairs4 {
            top: -4px;
            left: 16px;
            width: 150px;
            height: 0px;
            display: flex;
        }

        .square .stairs4 li {
            margin-right: 3.5px;
        }

        .planet {
            position: absolute;
            z-index: 10;
            width: 90px;
            height: 90px;
            border-radius: 50%;
            box-sizing: border-box;
        }

        .planet::before, .planet::after {
            content: "";
            position: absolute;
            z-index: 1;
            top: 50%;
            left: 50%;
            border-radius: 50%;
            transform: translate(-50%, -50%);
        }

        .planet::before {
            width: 70px;
            height: 70px;
        }

        .planet::after {
            width: 60px;
            height: 60px;
        }

        .planet1 {
            box-shadow: 0px 0px 30px #a3358c;
            bottom: 260px;
            left: 50px;
            border: 5px solid #a3358c;
            background-color: #cf3684;
        }

        .planet1::before {
            background-color: #fd8f5d;
        }

        .planet1::after {
            background-color: #ffdf70;
        }

        .planet2 {
            box-shadow: 0px 0px 30px #1383df;
            -moz-transform: scale(0.8);
            -ms-transform: scale(0.8);
            -webkit-transform: scale(0.8);
            transform: scale(0.8);
            bottom: 40px;
            right: 80px;
            border: 5px solid #1383df;
            background-color: #08abf3;
        }

        .planet2::before {
            background-color: #11c1f1;
        }

        .planet2::after {
            background-color: #22e5f1;
        }

        .planet3 {
            box-shadow: 0px 0px 30px #7a7afe;
            -moz-transform: scale(0.6);
            -ms-transform: scale(0.6);
            -webkit-transform: scale(0.6);
            transform: scale(0.6);
            top: 20px;
            right: 30px;
            border: 5px solid #7a7afe;
            background-color: #9a82ff;
        }

        .planet3::before {
            background-color: #b588ff;
        }

        .planet3::after {
            background-color: #d491ff;
        }

        .girl {
            position: relative;
            z-index: 99;
            width: 12px;
            height: 40px;
            top: 87px;
            left: -78px;
        }

        .girl .head {
            position: relative;
            z-index: 10;
            width: 10px;
            height: 10px;
            box-sizing: border-box;
            background-color: #fff;
            border-left: 3px solid #000;
            border-radius: 5px;
            -moz-transform: rotate(-20deg);
            -ms-transform: rotate(-20deg);
            -webkit-transform: rotate(-20deg);
            transform: rotate(-20deg);
        }

        .girl .head::before {
            content: "";
            position: absolute;
            left: -16px;
            top: 0px;
            width: 0px;
            height: 0px;
            display: block;
            border-right: 15px solid #fff;
            border-top: 5px solid transparent;
            border-bottom: 4px solid transparent;
        }

        .girl .body {
            position: absolute;
            top: 7px;
            left: -2px;
            width: 0px;
            height: 0px;
            box-sizing: border-box;
            border-bottom: 20px solid #fff;
            border-left: 8px solid transparent;
            border-right: 8px solid transparent;
            border-radius: 7px;
        }

        .girl .legs {
            position: absolute;
            bottom: 5px;
            left: 3px;
            width: 3px;
            height: 10px;
            border-left: 2px solid #fff;
            border-right: 2px solid #fff;
        }
    </style>
</head>
<body>
<!-- partial:index.partial.html -->
<div class="scenery">
    <div class="stars">
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
    </div>
    <div class="stars2">
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
        <div class="star2"></div>
    </div>
    <div class="planet planet1"></div>
    <div class="planet planet2"></div>
    <div class="planet planet3"></div>
    <div class="girl">
        <div class="head"></div>
        <div class="body"></div>
        <div class="legs"></div>
    </div>
    <div class="title">
        <!-- <h1><span>T</span> -->
        <h1><span></span>
            <div class="square">
                <div class="light light1"></div>
                <div class="light light2"></div>
                <div class="light light3"></div>
                <div class="light light4"></div>
                <div class="shadow shadow1"></div>
                <div class="shadow shadow2"></div>
                <ul class="stairs1">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <ul class="stairs2">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <ul class="stairs3">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <ul class="stairs4">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <!-- </div><span>T</span> -->
            </div>
            <span></span>
        </h1>
    </div>
    <div class="message">
        <h2><b style="color:yellow"><?= $MissingPageName ?></b> page is currently still under construction <i
                    class="fas fa-wrench"></i></h2>
    </div>

    <div class="message">
        <h3>You will be automatically redirected in <b id="countdown" style="color: yellow">10s</b></h3>
    </div>
    <!-- <text class="email">nannie@vt.edu</text> -->

    <!--  <div class="message">-->
    <!--    <h4>Contact administrator <a href="mailto:nannie@vt.edu?subject=Contact" target="_top" class="yellow">nannie@vt.edu </a> for more details</h4>-->
    <!--  </div>-->
    <div class="action">
        <button onClick="window.location.reload();" class="refresh">Refresh</button>
        <button onclick="window.location.href='<?= BASE_URL_NO_WEB ?>'" class="continue">Return</button>
    </div>
</div>
<!-- partial -->s
<script>
    var timeleft = 9;
    var downloadTimer = setInterval(function () {
        if (timeleft <= 0) {
            clearInterval(downloadTimer);
        }
        // document.getElementById("countdown").value = 10 - timeleft;
        document.getElementById("countdown").innerText = (timeleft).toString() + "s";
        timeleft -= 1;
        if (timeleft < 0) {
            window.location.href = '<?=BASE_URL_NO_WEB?>';
        }
    }, 1000);
</script>
</body>
</html>
